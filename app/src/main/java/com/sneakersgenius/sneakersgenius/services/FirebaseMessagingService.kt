package com.sneakersgenius.sneakersgenius.services

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.pages.landing.LandingActivity
import com.sneakersgenius.sneakersgenius.utilities.NotificationChannels
import timber.log.Timber


/**
 * Created by Ondrej Synak on 6/10/18.
 */
class FirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Timber.d("onMessageReceived() $remoteMessage")

        if (remoteMessage.data.isNotEmpty()) {

        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Timber.d("Message Notification Body: ${remoteMessage.notification!!.body}")
            remoteMessage.notification?.let { sendNotification(it) }
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     */
    @SuppressLint("NewApi")
    private fun sendNotification(notification: RemoteMessage.Notification) {
        val intent = Intent(this, LandingActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannels(this)
        }

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, NotificationChannels.DEFAULT_CHANNEL_ID)
                .setSmallIcon(R.drawable.jordan1)
                .setContentTitle(notification.title)
                .setContentText(notification.body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(0, notificationBuilder.build())
    }
}