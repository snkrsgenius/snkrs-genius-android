package com.sneakersgenius.sneakersgenius.services

import com.google.firebase.iid.FirebaseInstanceIdService
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import timber.log.Timber
import com.google.firebase.iid.FirebaseInstanceId


/**
 * Created by Ondrej Synak on 6/10/18.
 */
class FirebaseInstanceService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        val userID = App.appComponent.provideSharedPreferencesHelper().loadString(SharedPreferencesHelper.PREF_KEY_USER_ID)
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Timber.d("onTokenRefresh() serverID: $userID, firebaseToken: $refreshedToken")

        sendRegistrationToServer(userID, refreshedToken)
    }


    private fun sendRegistrationToServer(userId: String?, refreshedToken: String?) {

        //TODO
    }
}