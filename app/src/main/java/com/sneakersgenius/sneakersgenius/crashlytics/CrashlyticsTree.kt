package com.sneakersgenius.sneakersgenius.crashlytics

import android.util.Log
import com.crashlytics.android.Crashlytics
import timber.log.Timber

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class CrashlyticsTree : Timber.DebugTree() {
    /**
     * These TAGs are ignored
     */
    private val spammers = listOf(
            "CameraStatistics",
            "EglRenderer",
            "TimberLogger"
    )

    /**
     * Logs to Crashlytics
     *
     * If priority is warning or higher, and some throwable is passed, send non-fatal report to Crashlytics
     */
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (!spammers.contains(tag)) {
            //Crashlytics.log("$tag: $message")
            if (priority >= Log.WARN) {
                //Crashlytics.logException(t)
            }
        }
    }
}