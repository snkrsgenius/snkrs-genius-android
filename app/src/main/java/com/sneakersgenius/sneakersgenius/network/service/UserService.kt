package com.sneakersgenius.sneakersgenius.network.service

import com.sneakersgenius.sneakersgenius.network.RestHelper
import com.sneakersgenius.sneakersgenius.network.restModels.User
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by Ondrej Synak on 5/12/18.
 */
interface UserService {

    @POST("${RestHelper.VERSION}/users")
    fun getUserId(@Body user: User): Single<Response<User>>
}
