package com.sneakersgenius.sneakersgenius.network.service

import com.sneakersgenius.sneakersgenius.network.RestHelper
import com.sneakersgenius.sneakersgenius.network.restModels.*
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by Ondrej Synak on 5/12/18.
 */
interface GameService {
    @GET("${RestHelper.VERSION}/games")
    fun getGames(): Single<Response<List<Game>>>

    @GET("${RestHelper.VERSION}/games/mostPlayed")
    fun getMostPlayedGames(@Query("limit") limit: Int): Single<Response<List<Game>>>

    @GET("${RestHelper.VERSION}/games/{id_game}/detail")
    fun getSingleGame(@Path("id_game") gameId: String): Single<Response<GameDetail>>

    @GET("${RestHelper.VERSION}/leaderboards/{user_id}/statistics")
    fun getLandingLeaderboards(@Path("user_id") userId: String): Single<Response<List<LeaderBoardItemLandingPage>>>

    @GET("${RestHelper.VERSION}/leaderboards/{id_game}")
    fun getGameLeaderBoard(@Path("id_game") gameId: String): Single<Response<List<LeaderBoardItem>>>

    @PUT("${RestHelper.VERSION}/leaderboards/{id_game}")
    fun postGameScore(@Path("id_game") gameId: String, @Body gameScore: GameScore): Single<Response<Error>>
}