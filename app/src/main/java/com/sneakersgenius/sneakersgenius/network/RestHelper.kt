package com.sneakersgenius.sneakersgenius.network

import com.google.gson.Gson
import com.sneakersgenius.sneakersgenius.network.restModels.GameScore
import com.sneakersgenius.sneakersgenius.network.restModels.User
import com.sneakersgenius.sneakersgenius.network.service.GameService
import com.sneakersgenius.sneakersgenius.network.service.UserService
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 5/12/18.
 */
@Singleton
class RestHelper(retrofit: Retrofit, gson: Gson) {
    companion object {
        const val VERSION = "v1" //Maybe move it somewhere else?
    }

    //region Retrofit Services

    private val userService: UserService = retrofit.create(UserService::class.java)
    private val gameService: GameService = retrofit.create(GameService::class.java)

    //endregion

    fun getUser(user: User) = userService.getUserId(user)

    fun loadGames() = gameService.getGames()

    fun loadMostPlayedGames(limit: Int = 5) = gameService.getMostPlayedGames(limit)

    fun loadLeaderBoardForGame(gameId: String) = gameService.getGameLeaderBoard(gameId)

    fun loadLandingLeaderboardsForUser(userId: String) = gameService.getLandingLeaderboards(userId)

    fun loadGameDetail(gameId: String) = gameService.getSingleGame(gameId)

    fun saveGameScore(gameId: String, gameScore: GameScore) = gameService.postGameScore(gameId, gameScore)
}