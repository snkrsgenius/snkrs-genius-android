package com.sneakersgenius.sneakersgenius.network.restModels

/**
 * Created by Ondrej Synak on 5/12/18.
 */
data class User(val id: String?, val email: String, val firstName: String, val lastName: String, val photoUrl: String, val token: String? = null)

data class Game(val id: String, val name: String, val description: String, val imageUrl: String, val minifiedImageUrl: String, val players: MutableList<LeaderBoardItem>? = null)

data class LeaderBoardItem(val player: User, val bestScore: Int, val lastScore: Int, val gameId: String, val position: Int) : Comparable<LeaderBoardItem> {
    override fun compareTo(other: LeaderBoardItem): Int {
        return other.bestScore.compareTo(bestScore)
    }
}

data class LeaderBoardItemLandingPage(val gameName: String, val gameID: String, val total: Int, val position: Int, val photoURL: String)


data class Error(val message: String, val exception: String, val status: Int, val error: String, val path: String)

data class GameDetail(val name: String, val description: String, var sneakers: List<Sneakers>)

data class Sneakers(val id: Int, val name: String, val photoUrl: String)

data class GameScore(val score: Int, val playerId: String)