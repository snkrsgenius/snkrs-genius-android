package com.sneakersgenius.sneakersgenius.dependencyInjection.module

import android.content.Context
import com.google.gson.Gson
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 5/12/18.
 */
@Module
class AppModule(val context: Context) {

    @Provides
    fun provideContext(): Context = context

    @Provides
    @Singleton
    fun provideSharedPreferences(gson: Gson) = SharedPreferencesHelper(context, gson)
}