package com.sneakersgenius.sneakersgenius.dependencyInjection

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class SharedPreferencesHelper(context: Context, private val gson: Gson) : SharedPreferences by PreferenceManager.getDefaultSharedPreferences(context) {
    companion object {
        const val PREF_KEY_USER_ID = "pref_key_user_id"
        const val PREF_KEY_FIRST_GAME = "pref_key_first_game"
        const val PREF_KEY_RATED = "pref_key_rated"
        const val PREF_KEY_RATE_LATER = "pref_key_rate_later"
        const val PREF_KEY_USER_PHOTO = "pref_key_user_photo_url"
    }

    /**
     * General Save function to shared preferences.
     *
     * @param data Saved data.
     * @param key  Shared pref. key
     * @param <T>  Generic Serialization class.
    </T> */
    fun <T> save(data: T, key: String) {
        edit().putString(key, gson.toJson(data)).apply()
    }

    /**
     * General Load function from shared preferences.
     *
     * @param key   Shared pref. key
     * @param clazz Class for deserialization.
     * @param <T>
     * @return Data.
    </T> */
    fun <T> load(key: String, clazz: Class<T>): T {
        return gson.fromJson(getString(key, ""), clazz)
    }

    fun saveBoolean(key: String, state: Boolean) {
        edit().putBoolean(key, state).apply()
    }

    fun loadBoolean(key: String): Boolean {
        return getBoolean(key, false)
    }

    fun saveInt(key: String, state: Int) {
        edit().putInt(key, state).apply()
    }

    fun loadInt(key: String): Int {
        return getInt(key, -1)
    }

    fun saveLong(key: String, state: Long) {
        edit().putLong(key, state).apply()
    }

    fun loadLong(key: String): Long {
        return getLong(key, -1)
    }

    fun saveString(key: String, state: String?) {
        edit().putString(key, state).apply()
    }

    fun loadString(key: String): String? {
        return getString(key, null)
    }
}