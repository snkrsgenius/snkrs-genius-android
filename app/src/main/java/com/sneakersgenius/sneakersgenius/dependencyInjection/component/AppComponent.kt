package com.sneakersgenius.sneakersgenius.dependencyInjection.component

import com.sneakersgenius.sneakersgenius.database.AppRoomDatabase
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import com.sneakersgenius.sneakersgenius.dependencyInjection.module.AppModule
import com.sneakersgenius.sneakersgenius.dependencyInjection.module.DatabaseModule
import com.sneakersgenius.sneakersgenius.dependencyInjection.module.NetworkModule
import com.sneakersgenius.sneakersgenius.pages.BaseActivity
import com.sneakersgenius.sneakersgenius.pages.gameDetail.GameDetailViewModel
import com.sneakersgenius.sneakersgenius.pages.gameLeaderboard.GameLeaderboardViewModel
import com.sneakersgenius.sneakersgenius.pages.gameOverview.GameOverviewViewModel
import com.sneakersgenius.sneakersgenius.pages.landing.LandingViewModel
import com.sneakersgenius.sneakersgenius.pages.login.LoginViewModel
import com.sneakersgenius.sneakersgenius.pages.score.ScoreViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 5/12/18.
 */
@Component(modules = [AppModule::class, NetworkModule::class, DatabaseModule::class])
@Singleton
interface AppComponent {

    fun provideSharedPreferencesHelper(): SharedPreferencesHelper

    fun provideDatabase(): AppRoomDatabase

    fun inject(loginViewModel: LoginViewModel)
    fun inject(baseActivity: BaseActivity)
    fun inject(gameOverviewViewModel: GameOverviewViewModel)
    fun inject(gameLeaderboardViewModel: GameLeaderboardViewModel)
    fun inject(gameDetailViewModel: GameDetailViewModel)
    fun inject(scoreViewModel: ScoreViewModel)
    fun inject(landingViewModel: LandingViewModel)
}