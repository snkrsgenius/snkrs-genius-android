package com.sneakersgenius.sneakersgenius.dependencyInjection.module

import android.arch.persistence.room.Room
import android.content.Context
import com.sneakersgenius.sneakersgenius.database.AppRoomDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 5/12/18.
 */
@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppRoomDatabase =
            Room.databaseBuilder(context, AppRoomDatabase::class.java, "iqrf_repository")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()
}