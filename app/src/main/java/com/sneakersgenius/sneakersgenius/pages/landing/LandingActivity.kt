package com.sneakersgenius.sneakersgenius.pages.landing

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import com.sneakersgenius.sneakersgenius.network.restModels.Game
import com.sneakersgenius.sneakersgenius.network.restModels.LeaderBoardItemLandingPage
import com.sneakersgenius.sneakersgenius.pages.BaseActivity
import com.sneakersgenius.sneakersgenius.pages.gameLeaderboard.GameLeaderboardActivity
import com.sneakersgenius.sneakersgenius.pages.gameOverview.GameOverviewActivity
import com.sneakersgenius.sneakersgenius.pages.landing.components.gameScroll.LandingGameAdapter
import com.sneakersgenius.sneakersgenius.pages.landing.components.leaderboardScroll.LandingLeaderboardsAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.landing_activity.*
import timber.log.Timber

/**
 * Created by Ondrej Synak on 6/9/18.
 */
class LandingActivity : BaseActivity() {

    private lateinit var landingViewModel: LandingViewModel
    private val onGameLandingItemListener = object : LandingGameAdapter.OnGameLandingItemListener {
        override fun onItemClicked(game: Game) {
            Timber.v("onItemClicked() $game")
            startActivity(
                    Intent(this@LandingActivity, GameLeaderboardActivity::class.java)
                            .putExtra(GameLeaderboardActivity.GAME_NAME, game.name)
                            .putExtra(GameLeaderboardActivity.GAME_ID, game.id)
            )

            overridePendingTransition(R.anim.slide_in_right, R.anim.no_anim)
        }
    }
    private val onLeaderboardsLandingItemListener = object : LandingLeaderboardsAdapter.OnLeaderboardsLandingItemListener {
        override fun onItemClicked(item: LeaderBoardItemLandingPage) {
            Timber.v("onItemClicked() $item")
            startActivity(
                    Intent(this@LandingActivity, GameLeaderboardActivity::class.java)
                            .putExtra(GameLeaderboardActivity.GAME_NAME, item.gameName)
                            .putExtra(GameLeaderboardActivity.GAME_ID, item.gameID)
            )

            overridePendingTransition(R.anim.slide_in_right, R.anim.no_anim)
        }
    }

    /* Public Methods *****************************************************************************************************************************************/

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.landing_activity)

        landingViewModel = ViewModelProviders.of(this).get(LandingViewModel::class.java)

        bindObservers()
        setupViews()
    }

    override fun onResume() {
        super.onResume()

        most_played_games_container.setProgressBarVisibility(true)
        leaderboards_container.setProgressBarVisibility(true)
        landingViewModel.loadData()
    }


    /* Private Methods ****************************************************************************************************************************************/

    private fun bindObservers() {
        Timber.v("bindObservers()")

        landingViewModel.gamesLiveData.observe(this, Observer {
            most_played_games_container.setItems(it!!.toMutableList())
            most_played_games_container.setProgressBarVisibility(false)
        })

        landingViewModel.leaderboardLiveData.observe(this, Observer {
            if (it!!.isEmpty()) {
                leaderboards_container.visibility = View.GONE
            } else {
                leaderboards_container.visibility = View.VISIBLE
                leaderboards_container.setItems(it.toMutableList())
                leaderboards_container.setProgressBarVisibility(false)
            }
        })
    }

    private fun setupViews() {
        Timber.v("setupViews")

        Picasso.get().load(sharedPreferencesHelper.loadString(SharedPreferencesHelper.PREF_KEY_USER_PHOTO)).into(profile_image_view)

        val headerText = getString(R.string.rate_dialog_header)
        val styledText = SpannableString(headerText.toUpperCase())
        styledText.setSpan(ForegroundColorSpan(ContextCompat.getColor(this, R.color.jay_gay_yellow)), 16, headerText.length, Spannable
                .SPAN_EXCLUSIVE_EXCLUSIVE)

        rate_us_header_text_view.text = styledText

        play_button.setOnClickListener {
            startActivity(Intent(this, GameOverviewActivity::class.java))
        }

        rate_us_button.setOnClickListener {
            sharedPreferencesHelper.saveBoolean(SharedPreferencesHelper.PREF_KEY_RATED, true)
            goToStore()
        }

        most_played_games_container.setItemClickListener(onGameLandingItemListener)
        leaderboards_container.setItemClickListener(onLeaderboardsLandingItemListener)
    }

    private fun goToStore() {
        Intent(Intent.ACTION_VIEW).let {
            it.data = Uri.parse("market://details?id=$packageName")
            if (it.resolveActivity(packageManager) == null) {
                it.data = Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
            }
            startActivity(it)
        }
    }
}