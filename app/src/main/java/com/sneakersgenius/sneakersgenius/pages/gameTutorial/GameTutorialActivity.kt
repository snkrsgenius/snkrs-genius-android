package com.sneakersgenius.sneakersgenius.pages.gameTutorial

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import com.sneakersgenius.sneakersgenius.pages.BaseActivity
import com.sneakersgenius.sneakersgenius.pages.gameDetail.GameDetailActivity
import kotlinx.android.synthetic.main.game_tutorial_activity.*
import timber.log.Timber

/**
 * Created by Ondrej Synak on 5/13/18.
 */
class GameTutorialActivity : BaseActivity() {

    companion object {
        const val GAME_ID = "game_id"
        const val GAME_NAME = "game_name"
        const val GAME_HIGHSCORE = "game_highscore"
        const val CURRENT_PAGE = "current_page"
    }

    /**
     * Pages of view switcher.
     */
    enum class Page {
        PREVIOUS,
        FIRST,
        SECOND
    }

    /* Private Attributes *************************************************************************************************************************************/

    private var gameId: String? = null
    private var gameName: String? = null
    private var currentPage: Page = Page.FIRST
    private var highScore = 0

    /* Public Methods *****************************************************************************************************************************************/

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.game_tutorial_activity)

        // Decide which state bundle should be used when loading arguments.
        loadArguments(savedInstanceState ?: intent.extras)

        setupViews()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString(GameTutorialActivity.GAME_ID, gameId)
        outState?.putString(GameTutorialActivity.GAME_NAME, gameName)
        outState?.putInt(GameTutorialActivity.GAME_HIGHSCORE, highScore)
        outState?.putInt(GameTutorialActivity.CURRENT_PAGE, currentPage.ordinal)
    }

    override fun onBackPressed() {
        when (currentPage) {
            Page.SECOND -> {
                currentPage = Page.FIRST
                goToPage(currentPage)
            }
            Page.FIRST -> {
                currentPage = Page.PREVIOUS
                goToPage(currentPage)
            }
            else -> {

            }
        }
    }

    /* Private Methods ****************************************************************************************************************************************/

    private fun setupViews() {
        Timber.v("setupViews")

        tutorial_next_button.setOnClickListener {
            currentPage = Page.SECOND
            goToPage(currentPage)
        }

        tutorial_start_button.setOnClickListener {
            sharedPreferencesHelper.saveBoolean(SharedPreferencesHelper.PREF_KEY_FIRST_GAME, true)
            startActivity(
                    Intent(this, GameDetailActivity::class.java)
                            .putExtra(GameDetailActivity.GAME_ID, gameId)
                            .putExtra(GameDetailActivity.HIGH_SCORE, 0)
            )
            finish()
        }
    }

    private fun goToPage(page: Page) {
        Timber.d("goToPage()")

        when {
            page === Page.SECOND -> duel_tutorial_view_switcher.showNext()
            page === Page.FIRST -> duel_tutorial_view_switcher.showPrevious()
            page === Page.PREVIOUS -> super.onBackPressed()
        }
    }


    /**
     * Loads arguments from SavedInstance or passed bundle.
     *
     * @param state Bundle with data.
     */
    private fun loadArguments(state: Bundle?) {
        // Load arguments.
        if (state != null) {
            if (state.containsKey(GameTutorialActivity.GAME_ID)) {
                gameId = state.getString(GameTutorialActivity.GAME_ID)
            }
            if (state.containsKey(GameTutorialActivity.GAME_NAME)) {
                gameName = state.getString(GameTutorialActivity.GAME_NAME)
            }
            if (state.containsKey(GameTutorialActivity.CURRENT_PAGE)) {
                currentPage = Page.values()[state.getInt(GameTutorialActivity.CURRENT_PAGE)]
            }
            if (state.containsKey(GameTutorialActivity.GAME_HIGHSCORE)) {
                highScore = state.getInt(GameTutorialActivity.GAME_HIGHSCORE)
            }
        }
    }
}