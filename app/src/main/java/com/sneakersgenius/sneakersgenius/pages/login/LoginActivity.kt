package com.sneakersgenius.sneakersgenius.pages.login

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.facebook.*
import com.facebook.login.LoginResult
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.pages.BaseActivity
import com.sneakersgenius.sneakersgenius.pages.gameOverview.GameOverviewActivity
import com.sneakersgenius.sneakersgenius.pages.landing.LandingActivity
import com.sneakersgenius.sneakersgenius.uiComponents.LoginDialog
import kotlinx.android.synthetic.main.login_activity_layot.*
import timber.log.Timber
import java.util.*

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class LoginActivity : BaseActivity() {

    companion object {
        private const val totalScrollTime = Long.MAX_VALUE
        private const val scrollPeriod = 50
        private const val heightToScroll = 3
    }

    /* Private Attributes *************************************************************************************************************************************/

    private val callbackManager: CallbackManager = CallbackManager.Factory.create()

    private val countDownTimer = object : CountDownTimer(totalScrollTime, scrollPeriod.toLong()) {
        override fun onTick(l: Long) {
            if (first_row_recycler_view != null && second_row_recycler_view != null && third_row_recycler_view != null) {
                third_row_recycler_view.smoothScrollBy(heightToScroll, 0)
                first_row_recycler_view!!.smoothScrollBy(heightToScroll, 0)
                second_row_recycler_view!!.smoothScrollBy(-heightToScroll, 0)
            }
        }

        override fun onFinish() {}
    }

    private val jordanFirstPhotoList = Arrays.asList(R.drawable.jordan3,
            R.drawable.jordan2, R.drawable.jordan4,
            R.drawable.jordan5, R.drawable.jordan6,
            R.drawable.jordan7, R.drawable.jordan8,
            R.drawable.jordan9, R.drawable.jordan10)

    private val airMaxPhotoList = Arrays.asList(R.drawable.airax98,
            R.drawable.airmax1, R.drawable.airmax90,
            R.drawable.airmax95, R.drawable.airmax96,
            R.drawable.airmax97, R.drawable.airmax180,
            R.drawable.airmax360, R.drawable.airmax2014,
            R.drawable.airmax21010, R.drawable.airmaxlight,
            R.drawable.airmaxpillar, R.drawable.airmaxtavas,
            R.drawable.airmaxtrainer1, R.drawable.airmaxtzone,
            R.drawable.airmaxzero)

    private val jordanSecondPhotoList = Arrays.asList(R.drawable.jordan11,
            R.drawable.jordan12, R.drawable.jordan13,
            R.drawable.jordan14, R.drawable.jordan15,
            R.drawable.jordan16, R.drawable.jordan17,
            R.drawable.jordan18, R.drawable.jordan19,
            R.drawable.jordan20)

    private lateinit var loginViewModel: LoginViewModel

    /* Public Methods *****************************************************************************************************************************************/

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, layoutResId = R.layout.login_activity_layot)

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        bindObservers()
        setupViews()
    }

    override fun onDestroy() {
        super.onDestroy()
        countDownTimer.cancel()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    fun setLoadingVisibility(visible: Boolean) {
        sign_in_loading_progress_bar.visibility = if (visible) View.VISIBLE else View.GONE
    }

    /* Private Method *****************************************************************************************************************************************/

    private fun bindObservers() {

        loginViewModel.internetErrorLiveData.observe(this, android.arch.lifecycle.Observer {
            if (it!!) {
                Toast.makeText(this, this.getString(R.string.network_offline),
                        Toast.LENGTH_SHORT).show()
                loginViewModel.internetErrorLiveData.value = false
            }
        })

        loginViewModel.userLiveData.observe(this, android.arch.lifecycle.Observer {
            startActivity(Intent(this, LandingActivity::class.java))
            setLoadingVisibility(false)
            finish()
        })
    }

    private fun setupViews() {
        Timber.v("setupViews()")

        setupEndlessRecyclerViews()
        setupFacebookLogin()

        gradient_text_view.post({
            val shader = LinearGradient(0f, 0f, gradient_text_view.width.toFloat(), gradient_text_view.paint.textSize, intArrayOf(Color.parseColor("#FFE240"), Color.parseColor("#FFA53D")),
                    floatArrayOf(0f, 1f), Shader.TileMode.CLAMP)
            gradient_text_view.paint.shader = shader
        })
    }

    private fun setupEndlessRecyclerViews() {
        // Setup top three recycler views as endless animations.


        with(first_row_recycler_view) {
            layoutManager = LinearLayoutManager(this@LoginActivity, LinearLayoutManager.HORIZONTAL, false)
            setHasFixedSize(true)
            adapter = EndlessAdapter(jordanFirstPhotoList)
        }

        with(second_row_recycler_view) {
            layoutManager = LinearLayoutManager(this@LoginActivity, LinearLayoutManager.HORIZONTAL, false)
            setHasFixedSize(true)
            adapter = EndlessAdapter(airMaxPhotoList)
        }

        with(third_row_recycler_view) {
            layoutManager = LinearLayoutManager(this@LoginActivity, LinearLayoutManager.HORIZONTAL, false)
            setHasFixedSize(true)
            adapter = EndlessAdapter(jordanSecondPhotoList)
        }

        third_row_recycler_view.post({
            first_row_recycler_view.scrollToPosition(Integer.MAX_VALUE / 2)
            second_row_recycler_view.scrollToPosition(Integer.MAX_VALUE / 2)
            third_row_recycler_view.scrollToPosition(Integer.MAX_VALUE / 2)
            countDownTimer.start()
        })
    }

    private fun setupFacebookLogin() {
        facebook_button.setOnClickListener {
            Timber.d("onFacebookButtonClick()")
            val accessToken = AccessToken.getCurrentAccessToken()
            if (accessToken != null) {
                showLogOutDialog(Profile.getCurrentProfile().name)
            } else {
                performClickOnFBButton()
            }
        }

        facebook_login_button.setReadPermissions("email")
        facebook_login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                Timber.d("FB Login successful: " + result.toString())
                loginViewModel.onUserContinue()
            }

            override fun onCancel() {
                Timber.w("FB Login canceled")
            }

            override fun onError(error: FacebookException?) {
                Timber.w("FB Login Error ")
                error?.printStackTrace()
                Toast.makeText(this@LoginActivity, this@LoginActivity.getString(R.string.network_offline), Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun performClickOnFBButton() {
        Timber.v("performClickOnFBButton()")

        facebook_login_button.performClick()
    }

    /**
     * Show dialog
     *
     * @param name User name
     */
    private fun showLogOutDialog(name: String) {
        Timber.d("showLogOutDialog()")

        val dialog = LoginDialog(this)
        dialog.setOnLogOutClickListener(View.OnClickListener {
            dialog.dismiss()
            loginViewModel.onUserLogout()
        })
        dialog.setOnOkButtonClickListener(View.OnClickListener {
            dialog.dismiss()
            loginViewModel.onUserContinue()
        })
        dialog.setOkButtonTitle(name)
        dialog.show()
    }
}