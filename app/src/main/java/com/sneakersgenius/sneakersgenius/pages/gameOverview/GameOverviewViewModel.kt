package com.sneakersgenius.sneakersgenius.pages.gameOverview

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.network.restModels.Game
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class GameOverviewViewModel : ViewModel() {

    init {
        App.appComponent.inject(this)
    }

    @Inject
    lateinit var gameOverviewRepository: GameOverviewRepository

    val gamesLiveData: MutableLiveData<List<Game>> = MutableLiveData()
    val loadingInProgressLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val errorVisibility: MutableLiveData<Boolean> = MutableLiveData()

    /* Private Attributes *************************************************************************************************************************************/

    private var dataLoadingDisposable: Disposable? = null

    /* Public Methods *****************************************************************************************************************************************/

    fun loadData() {
        val firstTime = !(gamesLiveData.value != null && gamesLiveData.value!!.isNotEmpty())

        loadingInProgressLiveData.postValue(firstTime)
        errorVisibility.postValue(false)

        dataLoadingDisposable?.dispose()

        dataLoadingDisposable = gameOverviewRepository.loadData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    gamesLiveData.postValue(it)
                    loadingInProgressLiveData.postValue(false)
                }, {
                    Timber.e(it)
                    loadingInProgressLiveData.postValue(false)
                    errorVisibility.postValue(true)
                })
    }

    override fun onCleared() {
        dataLoadingDisposable?.dispose()

        super.onCleared()
    }
}