package com.sneakersgenius.sneakersgenius.pages.gameOverview

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.network.restModels.Game
import com.sneakersgenius.sneakersgenius.network.restModels.LeaderBoardItem
import com.sneakersgenius.sneakersgenius.uiComponents.GameOverviewPlayerLinearLayout
import com.squareup.picasso.Picasso
import timber.log.Timber

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class GameOverviewAdapter(
        private var games: List<Game> = arrayListOf(),
        private val onGameOverviewItemListener: OnGameOverviewItemListener,
        private val userId: String) : RecyclerView.Adapter<GameOverviewAdapter.OverviewDuelViewHolder>() {

    private val OVERVIEW_DUEL = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OverviewDuelViewHolder {
        return OverviewDuelViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.game_overview_item, parent, false))
    }

    override fun onBindViewHolder(holder: OverviewDuelViewHolder, position: Int) {
        Timber.d("onBindViewHolder $position")

        val selectedGame = games[position]
        holder.bind(position, selectedGame)
    }

    override fun getItemCount(): Int = games.size

    override fun getItemViewType(position: Int): Int = OVERVIEW_DUEL

    fun setNewGames(newGames: List<Game>) {
        games = newGames
        notifyDataSetChanged()
    }

    /**
     * ViewHolder for overview duel.
     */
    inner class OverviewDuelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(position: Int, game: Game) {
            itemView.findViewById<TextView>(R.id.overview_game_title_text_view).text = game.name
            itemView.findViewById<View>(R.id.overview_duel_card).setOnClickListener {
                onGameOverviewItemListener.onItemClicked(game)
            }
            setGameImage(game)

            setLeaderBoards(game.players)
        }

        private fun setLeaderBoards(players: List<LeaderBoardItem>?) {
            when {
                players == null || players.isEmpty()-> {
                    itemView.findViewById<GameOverviewPlayerLinearLayout>(R.id.overview_duel_first_player).clearView()
                    itemView.findViewById<GameOverviewPlayerLinearLayout>(R.id.overview_duel_second_player).clearView()
                }
                players.size == 1 -> {
                    with(itemView.findViewById<GameOverviewPlayerLinearLayout>(R.id.overview_duel_first_player)) {
                        setPlayerName("${players[0].player.firstName} ${players[0].player.lastName}")
                        setPlayerScore(players[0].bestScore)
                        setPlayerImage(players[0].player.photoUrl)
                        setPlayerPosition(players[0].position)
                    }
                    itemView.findViewById<GameOverviewPlayerLinearLayout>(R.id.overview_duel_second_player).clearView()
                }
                else -> {
                    var firstPlayer = 0
                    var secondPlayer = 1
                    val playersItem = players.firstOrNull { it.player.id == userId }

                    if (playersItem == null) {
                        firstPlayer = players.size - 2
                        secondPlayer = firstPlayer + 1
                    } else {
                        val currentPlayerPosition = players.indexOf(playersItem)
                        if (currentPlayerPosition != 0) {
                            firstPlayer = currentPlayerPosition
                            secondPlayer = currentPlayerPosition + 1
                        }
                    }

                    with(itemView.findViewById<GameOverviewPlayerLinearLayout>(R.id.overview_duel_first_player)) {
                        setPlayerName("${players[firstPlayer].player.firstName} ${players[firstPlayer].player.lastName}")
                        setPlayerScore(players[firstPlayer].bestScore)
                        setPlayerImage(players[firstPlayer].player.photoUrl)
                        setPlayerPosition(players[firstPlayer].position)
                    }
                    with(itemView.findViewById<GameOverviewPlayerLinearLayout>(R.id.overview_duel_second_player)) {
                        setPlayerName("${players[secondPlayer].player.firstName} ${players[secondPlayer].player.lastName}")
                        setPlayerScore(players[secondPlayer].bestScore)
                        setPlayerImage(players[secondPlayer].player.photoUrl)
                        setPlayerPosition(players[secondPlayer].position)
                    }
                }
            }
        }

        private fun setGameImage(game: Game) {
            Timber.v("setGameImage() game - $game")
            if (game.imageUrl.isNotEmpty()) {
                Picasso.get()
                        .load(game.imageUrl)
                        .placeholder(R.drawable.lobby_placeholder1)
                        .into(itemView.findViewById<ImageView>(R.id.overview_game_main_image_view))
            }
        }

    }

    interface OnGameOverviewItemListener {
        fun onItemClicked(game: Game)
    }
}
