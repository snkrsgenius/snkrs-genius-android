package com.sneakersgenius.sneakersgenius.pages.gameDetail

import android.arch.lifecycle.ViewModel
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.CountDownTimer
import android.os.Handler
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.network.restModels.GameDetail
import com.sneakersgenius.sneakersgenius.network.restModels.Sneakers
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.lang.Exception
import java.lang.ref.WeakReference
import java.util.*
import javax.inject.Inject

/**
 * Created by Ondrej Synak on 5/13/18.
 */
class GameDetailViewModel : ViewModel() {

    companion object {
        /**
         * Time between each question
         */
        const val TIME_BETWEEN_QUESTIONS = 1000

        /**
         * Time for each question, needed to be time + 2s.
         */
        const val TIME_FOR_QUESTION = 7000

        /**
         * How often does CountDownTimer ticks.
         */
        const val TIMER_TICK = 50
    }

    init {
        App.appComponent.inject(this)
        initialize()
    }

    @Inject
    lateinit var gameDetailRepository: GameDetailRepository
    var view: WeakReference<GameDetailActivity>? = null
    var highScore = 0

    /* Private Attributes *************************************************************************************************************************************/

    private lateinit var countDownTimer: CountDownTimer
    private var tick = 0
    private var dataLoadingDisposable: Disposable? = null

    /**
     * Counter of image loaded.
     */
    private var imageLoadedCounter: Int = 0

    /**
     * Overall image counter.
     */
    private var overallImageCount: Int = 0

    /**
     * Needed for keeping strong reference to Target for picasso to properly return callback with bitmap.
     */
    private var imageTargets: MutableSet<Target>? = null

    /**
     * Maximum amount of questions in this game.
     */
    private var maxQuestions = 0

    /**
     * Current question number
     */
    private var currentQuestionCounter: Int = 0

    /**
     * Flag which question is right
     */
    private var currentRightAnswer: Int = 0

    /**
     * Score overall for this game
     */
    private var overallScore = 0

    /**
     * Score for current question
     */
    private var currentQuestionScore: Int = 0

    /**
     * Current game.
     */
    private var gameDetail: GameDetail? = null

    private var endReason = 0

    /* Public Methods *****************************************************************************************************************************************/

    fun onPause() {
        countDownTimer.cancel()
    }


    /**
     * Event when first card is touched.
     */
    fun onDuelFirstCardTouched() {
        view?.get()?.setAnswersClickable(false)
        if (currentRightAnswer == 0) {
            overallScore += currentQuestionScore
            endReason = if (overallScore > highScore) 0 else 1
            view?.get()?.onFirstAnswerGood(currentQuestionScore)
        } else {
            view?.get()?.onFirstAnswerWrong()
            endReason = 2
            currentQuestionCounter = maxQuestions
        }
        restartQuestionLayout(TIME_BETWEEN_QUESTIONS)
    }

    /**
     * Event when second card is touched.
     */
    fun onDuelSecondCardTouched() {
        view?.get()?.setAnswersClickable(false)
        if (currentRightAnswer == 1) {
            overallScore += currentQuestionScore
            endReason = if (overallScore > highScore) 0 else 1
            view?.get()?.onSecondAnswerGood(currentQuestionScore)
        } else {
            view?.get()?.onSecondAnswerWrong()
            endReason = 2
            currentQuestionCounter = maxQuestions
        }
        restartQuestionLayout(TIME_BETWEEN_QUESTIONS)
    }

    /**
     * Clears previous question
     */
    fun onPreviousDuelQuestionCleared() {
        view?.get()?.setFirstAnswerImage(gameDetail!!.sneakers[(currentQuestionCounter - 1) * 2].photoUrl)
        view?.get()?.setSecondAnswerImage(gameDetail!!.sneakers[(currentQuestionCounter - 1) * 2 + 1].photoUrl)
        view?.get()?.initNewQuestion()
    }

    /**
     * Prepares new question
     */
    fun onNewDuelQuestionPrepared() {
        view?.get()?.setAnswersClickable(true)

        view?.get()?.setQuestionProgressBarVisibility(true)

        Handler().postDelayed({ countDownTimer.start() }, 50)
    }

    /**
     * Load game detail with id.
     */
    fun loadData(gameId: String) {
        Timber.v("loadData()")

        view?.get()?.setLoadingProgressBarVisibility(true)

        dataLoadingDisposable?.dispose()

        dataLoadingDisposable = gameDetailRepository.loadData(gameId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val data = it.sneakers.toMutableList()
                    data.shuffle(Random(System.nanoTime()))
                    it.sneakers = data

                    onDataLoaded(it)
                }, {
                    Timber.e(it)
                    view?.get()?.setLoadingProgressBarVisibility(false)
                    view?.get()?.onFailedLoadingGame(it.message ?: "")
                })
    }

    /* Private Methods ****************************************************************************************************************************************/

    private fun initialize() {
        currentQuestionScore = 100

        countDownTimer = object : CountDownTimer(TIME_FOR_QUESTION.toLong(), TIMER_TICK.toLong()) {
            override fun onTick(l: Long) {
                Timber.d("onTick()")
                currentQuestionScore -= 1
                if (tick != l.toInt() / 1000) {
                    tick = l.toInt() / 1000
                    // Sets progress on progress bar.
                    view?.get()?.setQuestionProgressBarTo(tick - 2, false)

                    // Question timed out.
                    if (tick == 1) {
                        onDuelTimeout()
                    }
                }
            }

            override fun onFinish() {}
        }
    }

    private fun onDuelTimeout() {
        view?.get()?.onFirstAnswerWrong()
        // End the game
        endReason = 3
        currentQuestionCounter = maxQuestions
        restartQuestionLayout(TIME_BETWEEN_QUESTIONS)
    }

    /**
     * Callback when data was loaded
     */
    private fun onDataLoaded(gameDetail: GameDetail) {
        Timber.v("onDataLoaded()")

        currentQuestionScore = 0
        maxQuestions = gameDetail.sneakers.size / 2
        overallScore = 0
        this.gameDetail = gameDetail
        preloadQuestionImages(gameDetail.sneakers)
    }

    private fun preloadQuestionImages(sneakersList: List<Sneakers>) {
        Timber.d("preloadQuestionImages()")

        // Sets current image loaded counter to 0 and max counter to item count
        imageLoadedCounter = 0
        overallImageCount = sneakersList.size - 1
        imageTargets = HashSet()

        // Load all images for current game
        for (sneakers in sneakersList) {
            Timber.d("preloadQuestionImages() ${sneakers.photoUrl}")
            if (sneakers.photoUrl.isEmpty()) {
                view?.get()?.onFailedImageLoading()
                return
            }

            val target = object : Target {
                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
//                    Timber.d("OnFailed()")
//                    if (sneakers.getImageArray() != null && sneakers.getImageArray().length !== 0) {
//                        imageLoadedCounter++
//
//                        // If all images are loaded, call callback
//                        if (imageLoadedCounter == overallImageCount + 1) {
//                            onQuestionImagesLoaded()
//                        }
//                    } else {
//                        // TODO Errror
//                    }
                    view?.get()?.onFailedLoadingGame("Failed to load duel images!")
                }

                override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {

                    imageLoadedCounter++

                    view?.get()?.setLoadingImagesNumber(imageLoadedCounter, overallImageCount + 1)

                    // If all images are loaded, call callback
                    if (imageLoadedCounter == overallImageCount + 1) {
                        onQuestionImagesLoaded()
                    }

                }

                override fun onPrepareLoad(placeHolderDrawable: Drawable) {

                }
            }

            (imageTargets as HashSet<Target>).add(target)

            Picasso.get().load(sneakers.photoUrl).placeholder(R.drawable.jordan1).into(target)
        }
    }

    /**
     * Callback when all imgaes are loaded
     */
    private fun onQuestionImagesLoaded() {
        Timber.d("onQuestionImagesLoaded()")

        view?.get()?.setLoadingProgressBarVisibility(false)

        initNextQuestion()
    }

    /**
     * Prepares next question
     */
    private fun initNextQuestion() {
        Timber.d("initNextQuestion()")
        currentQuestionCounter++
        currentQuestionScore = 100
        currentRightAnswer = if (Random().nextBoolean()) {
            1
        } else {
            0
        }

        tick = 0

        view?.get()?.setQuestionText(buildQuestionName(currentQuestionCounter, currentRightAnswer))
        view?.get()?.setQuestionCounter(buildQuestionCounter(currentQuestionCounter))
        startQuestion(currentQuestionCounter == 1)
    }

    /**
     * @param isFirstQuestion - if question is first one
     */
    private fun startQuestion(isFirstQuestion: Boolean) {
        Timber.d("startQuestion() $isFirstQuestion")

        // Cancels timer.
        countDownTimer.cancel()

        // If it is first questions. Prepare it, otherwise hide previous one and than
        // prepare new one.
        if (isFirstQuestion) {
            Timber.d("setImagesForInitQuestions()")
            view?.get()?.setFirstAnswerImage(gameDetail!!.sneakers[(currentQuestionCounter - 1) * 2].photoUrl)
            view?.get()?.setSecondAnswerImage(gameDetail!!.sneakers[(currentQuestionCounter - 1) * 2 + 1].photoUrl)
            view?.get()?.initNewQuestion()
        } else {
            view?.get()?.clearPreviousQuestion()
        }

        // Disables clicks on answers.
        view?.get()?.setAnswersClickable(false)
        view?.get()?.setQuestionProgressBarVisibility(true)

        view?.get()?.setQuestionProgressBarTo(0, false)
        view?.get()?.setQuestionProgressBarTo(5, true)
    }

    /**
     * Wait few after each question.
     *
     * @param timeToRestart - time to wait
     */
    private fun restartQuestionLayout(timeToRestart: Int) {
        Timber.d("restartQuestionLayout() %d", currentQuestionCounter)

        countDownTimer.cancel()

        view?.get()?.setOverallIQScore(overallScore)

        object : CountDownTimer(timeToRestart.toLong(), TIME_BETWEEN_QUESTIONS.toLong()) {
            override fun onTick(l: Long) {

            }

            override fun onFinish() {
                view?.get()?.clearAnswers()
                view?.get()?.setAnswersClickable(true)
                if (currentQuestionCounter != maxQuestions) {
                    initNextQuestion()
                } else {
                    stopGame()
                }
            }
        }.start()
    }

    /**
     * Stops duel.
     */
    private fun stopGame() {
        Timber.d("stopGame()")
        view?.get()?.onGameStopped(overallScore, endReason)
    }

    // region StringUtils

    /**
     * Build question counter from question count.
     *
     * @param questionCount current question
     *
     * @return formatted string
     */
    private fun buildQuestionCounter(questionCount: Int): String {
        val sb = StringBuilder()
        sb.append(questionCount)
        sb.append("/")
        sb.append(maxQuestions)
        return sb.toString()
    }

    /**
     * Build question name from current question counter and right answer index.
     *
     * @param questionCount current question index
     * @param rightAnswer right answer index
     *
     * @return formatted string containing question name
     */
    private fun buildQuestionName(questionCount: Int, rightAnswer: Int): String {

        val sb = StringBuilder()
        if (gameDetail != null) {
            sb.append(gameDetail!!.sneakers[(questionCount - 1) * 2 + rightAnswer].name)
        }
        sb.append(" ?")

        return sb.toString()
    }

    // endregion
}