package com.sneakersgenius.sneakersgenius.pages.landing.components.gameScroll

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.network.restModels.Game
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import timber.log.Timber

/**
 * Created by Ondrej Synak on 6/9/18.
 */
class LandingGameAdapter(var items: MutableList<Game> = arrayListOf()) : RecyclerView.Adapter<LandingGameAdapter.ItemViewHolder>() {

    var onGameClickedListener: OnGameLandingItemListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.landing_horizonal_scroll_component_item, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        Timber.d("onBindViewHolder ${items.size}")
        val item = items[position]

        holder.bind(item, position)
    }

    /**
     * Getter for current list size
     *
     * @return - list size
     */
    override fun getItemCount(): Int = items.size

    /**
     * Setter for adapter list
     */
    fun setNewItems(newItems: MutableList<Game>) {
        if (newItems.isNotEmpty()) {
            items = newItems
        } else {
            items.clear()
        }
        notifyDataSetChanged()
    }

    /**
     * ViewHolder for overview duel.
     */
    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Game, position: Int) {
            itemView.findViewById<TextView>(R.id.item_text_view).text = item.name

            Picasso.get().load(item.minifiedImageUrl).into(itemView.findViewById<CircleImageView>(R.id.main_image_view))

            itemView.setOnClickListener {
                onGameClickedListener?.onItemClicked(item)
            }
        }
    }

    interface OnGameLandingItemListener {
        fun onItemClicked(game: Game)
    }
}