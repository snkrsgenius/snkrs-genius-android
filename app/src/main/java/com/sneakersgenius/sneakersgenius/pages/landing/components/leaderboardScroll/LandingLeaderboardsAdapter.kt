package com.sneakersgenius.sneakersgenius.pages.landing.components.leaderboardScroll

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.network.restModels.LeaderBoardItemLandingPage
import com.sneakersgenius.sneakersgenius.utilities.OrdinalNumberUtil
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import timber.log.Timber


/**
 * Created by Ondrej Synak on 6/9/18.
 */
class LandingLeaderboardsAdapter(var items: MutableList<LeaderBoardItemLandingPage> = arrayListOf()) : RecyclerView.Adapter<LandingLeaderboardsAdapter.ItemViewHolder>() {

    var onLeaderboardsLandingItemListener: OnLeaderboardsLandingItemListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.leaderboards_landing_item, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        Timber.d("onBindViewHolder ${items.size}")
        val item = items[position]

        holder.bind(item, position)
    }

    /**
     * Getter for current list size
     *
     * @return - list size
     */
    override fun getItemCount(): Int = items.size

    /**
     * Setter for adapter list
     */
    fun setNewItems(newItems: MutableList<LeaderBoardItemLandingPage>) {
        if (newItems.isNotEmpty()) {
            items = newItems
        } else {
            items.clear()
        }
        notifyDataSetChanged()
    }

    /**
     * ViewHolder for overview duel.
     */
    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: LeaderBoardItemLandingPage, position: Int) {

            itemView.findViewById<TextView>(R.id.item_text_view).text = item.gameName
            itemView.findViewById<TextView>(R.id.position_text_view).text = getPositionString(item.position)
            itemView.findViewById<TextView>(R.id.total_text_view).text = itemView.context.getString(R.string.leaderboards_landing_total_format, item.total)
            Picasso.get().load(item.photoURL).placeholder(R.drawable.group).into(itemView.findViewById<CircleImageView>(R.id.main_leaderboard_image_view))

            itemView.setOnClickListener {
                onLeaderboardsLandingItemListener?.onItemClicked(item)
            }
        }
    }

    interface OnLeaderboardsLandingItemListener {
        fun onItemClicked(item: LeaderBoardItemLandingPage)
    }

    /**
     * Gets position as formatted string.
     * @param position - position of player
     * @return - formatted string
     */
    private fun getPositionString(position: Int): String {
        val sb = StringBuilder()
        sb.append(OrdinalNumberUtil.ordinal(position))
        return sb.toString()
    }
}