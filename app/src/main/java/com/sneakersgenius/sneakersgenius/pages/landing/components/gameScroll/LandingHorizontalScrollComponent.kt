package com.sneakersgenius.sneakersgenius.pages.landing.components.gameScroll

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.network.restModels.Game
import kotlinx.android.synthetic.main.landing_horizontal_scroll_component.view.*

/**
 * Created by Ondrej Synak on 6/9/18.
 */
class LandingHorizontalScrollComponent : LinearLayout {

    private var landingAdapter: LandingGameAdapter? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
        manageAttributes(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
        manageAttributes(attrs)
    }

    fun setItems(items: MutableList<Game> = arrayListOf()) {
        landingAdapter?.setNewItems(items)
    }

    fun setItemClickListener(listener: LandingGameAdapter.OnGameLandingItemListener) {
        landingAdapter?.onGameClickedListener = listener
    }

    fun setProgressBarVisibility(visible: Boolean) {
        progress_bar.visibility = if (visible) View.VISIBLE else View.GONE
    }

    /* Private Methods ******************************************************************************/

    /**
     * Initialize Widget
     */
    private fun init() {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.landing_horizontal_scroll_component, this)
        header_text_view.text = context.getString(R.string.landing_game_item_header)

        landingAdapter = LandingGameAdapter()
        with(recycler_view) {
            adapter = landingAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }
    }


    private fun manageAttributes(attr: AttributeSet) {

    }
}