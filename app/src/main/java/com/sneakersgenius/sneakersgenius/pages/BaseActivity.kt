package com.sneakersgenius.sneakersgenius.pages

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import com.sneakersgenius.sneakersgenius.pages.gameOverview.GameOverviewActivity
import com.sneakersgenius.sneakersgenius.pages.landing.LandingActivity
import com.sneakersgenius.sneakersgenius.pages.login.LoginActivity
import com.sneakersgenius.sneakersgenius.uiComponents.LoginDialog
import com.sneakersgenius.sneakersgenius.uiComponents.RateUsDialog
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import javax.inject.Inject

/**
 * Created by Ondrej Synak on 5/12/18.
 */
abstract class BaseActivity : AppCompatActivity() {

    @Inject
    open lateinit var sharedPreferencesHelper: SharedPreferencesHelper

    open fun onCreate(savedInstanceState: Bundle?, layoutResId: Int? = null) {
        super.onCreate(savedInstanceState)

        App.appComponent.inject(this)

        if (layoutResId != null) {
            setContentView(layoutResId)
            checkIfRateDialogShouldShowUp()
        } else {
            if (sharedPreferencesHelper.loadString(SharedPreferencesHelper.PREF_KEY_USER_ID) != null) {
                startActivity(Intent(this, LandingActivity::class.java))
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
            }
            finish()
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    private fun checkIfRateDialogShouldShowUp() {
        val rated = sharedPreferencesHelper.loadBoolean(SharedPreferencesHelper.PREF_KEY_RATED)
        val nextDate = sharedPreferencesHelper.loadLong(SharedPreferencesHelper.PREF_KEY_RATE_LATER)
        if (!rated && nextDate != -1L && nextDate < DateTime.now(DateTimeZone.UTC).millis) {
            with(RateUsDialog(this)) {
                setOnRateButtonClickListener(View.OnClickListener {
                    dismiss()
                    sharedPreferencesHelper.saveBoolean(SharedPreferencesHelper.PREF_KEY_RATED, true)
                    goToStore()
                })
                setOnLAterClickListener(View.OnClickListener {
                    dismiss()
                    sharedPreferencesHelper.saveLong(SharedPreferencesHelper.PREF_KEY_RATE_LATER, DateTime.now(DateTimeZone.UTC).plusDays(2).millis)
                })
                show()
            }
        }
    }

    private fun goToStore() {
        Intent(Intent.ACTION_VIEW).let {
            it.data = Uri.parse("market://details?id=$packageName")
            if (it.resolveActivity(packageManager) == null) {
                it.data = Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
            }
            startActivity(it)
        }
    }
}