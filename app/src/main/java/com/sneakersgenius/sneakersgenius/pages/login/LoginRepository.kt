package com.sneakersgenius.sneakersgenius.pages.login

import android.os.Bundle
import android.text.TextUtils
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.HttpMethod
import com.google.firebase.messaging.FirebaseMessaging
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import com.sneakersgenius.sneakersgenius.network.RestHelper
import com.sneakersgenius.sneakersgenius.network.restModels.User
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 5/12/18.
 */
@Singleton
class LoginRepository @Inject constructor(private val restHelper: RestHelper, private val sharedPreferenceHelper: SharedPreferencesHelper) {

    fun getFacebookUserData(): Single<User> {
        return Single.fromCallable<User> {

            Timber.d("getFacebookUserData()")

            val params = Bundle()
            params.putString("fields", "id,email,gender,cover,picture.type(large),first_name,last_name")

            val graphResponse = GraphRequest(AccessToken.getCurrentAccessToken(), "me", params, HttpMethod.GET).executeAndWait()

            if (graphResponse != null) {
                try {
                    val data = graphResponse.jsonObject
                    if (data.has("picture")) {

                        val profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url")
                        val email = data.getString("email")
                        val first_name = data.getString("first_name")
                        val last_name = data.getString("last_name")
                        val uid = data.getString("id")

                        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(first_name) && !TextUtils
                                        .isEmpty(last_name)) {

                            return@fromCallable User(null, email, first_name, last_name, profilePicUrl, uid)
                        }
                    } else {
                        throw Exception()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    throw e
                }
            }
            throw Exception()
        }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread()).flatMap {
                    registerUser(it)
                }
                .map {
                    if (it.isSuccessful) {
                        sharedPreferenceHelper.saveString(SharedPreferencesHelper.PREF_KEY_USER_ID, it.body()?.id)
                        sharedPreferenceHelper.saveString(SharedPreferencesHelper.PREF_KEY_USER_PHOTO, it.body()?.photoUrl)
                        FirebaseMessaging.getInstance().isAutoInitEnabled = true
                        it.body()!!
                    } else {
                        throw Exception()
                    }
                }
//                .onErrorResumeNext {
//                    Single.just(getMockedData())
//                }
    }

    private fun registerUser(user: User): Single<Response<User>> = restHelper.getUser(user)

    private fun getMockedData(): User {
        val user = User("Jakub", "jakub.sobotka@mail.com", "JAkub", "Sobotka", "Dont have")
        sharedPreferenceHelper.saveString(SharedPreferencesHelper.PREF_KEY_USER_ID, user.id)
        return user
    }

}