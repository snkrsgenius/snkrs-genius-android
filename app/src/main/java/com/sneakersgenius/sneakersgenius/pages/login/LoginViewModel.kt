package com.sneakersgenius.sneakersgenius.pages.login

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.facebook.login.LoginManager
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.network.restModels.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class LoginViewModel : ViewModel() {

    init {
        App.appComponent.inject(this)
    }

    @Inject
    lateinit var loginRepository: LoginRepository
    val internetErrorLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val userLiveData: MutableLiveData<User> = MutableLiveData()

    /* Private Attributes *************************************************************************************************************************************/

    private var loadUserDisposable: Disposable? = null

    /* Public Methods *****************************************************************************************************************************************/

    fun onUserLogout() {
        Timber.d("onUserLogOut()")

        LoginManager.getInstance().logOut()
    }

    fun onUserContinue() {
        Timber.d("onUserContinue()")

        loadUserDisposable?.dispose()

        loadUserDisposable = loginRepository.getFacebookUserData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    userLiveData.postValue(it)
                }, {
                    internetErrorLiveData.postValue(true)
                })
    }

    override fun onCleared() {
        loadUserDisposable?.dispose()
        super.onCleared()
    }
}