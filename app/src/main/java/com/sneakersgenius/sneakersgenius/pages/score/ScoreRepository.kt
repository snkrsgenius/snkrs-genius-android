package com.sneakersgenius.sneakersgenius.pages.score

import com.sneakersgenius.sneakersgenius.network.RestHelper
import com.sneakersgenius.sneakersgenius.network.restModels.GameScore
import com.sneakersgenius.sneakersgenius.pages.BaseRepository
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 5/13/18.
 */
@Singleton
class ScoreRepository @Inject constructor(private val restHelper: RestHelper) {

    fun saveData(gameId: String, gameScore: GameScore): Single<*> {
        return restHelper.saveGameScore(gameId, gameScore)
                .map {
                    if (it.isSuccessful) {
                        it.body()!!
                    } else {
                        throw Exception()
                    }
                }
    }
}