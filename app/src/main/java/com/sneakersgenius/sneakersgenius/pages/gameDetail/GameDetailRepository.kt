package com.sneakersgenius.sneakersgenius.pages.gameDetail

import com.sneakersgenius.sneakersgenius.network.RestHelper
import com.sneakersgenius.sneakersgenius.network.restModels.GameDetail
import com.sneakersgenius.sneakersgenius.network.restModels.Sneakers
import com.sneakersgenius.sneakersgenius.pages.BaseRepository
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 5/13/18.
 */
@Singleton
class GameDetailRepository @Inject constructor(private val restHelper: RestHelper) : BaseRepository<Single<GameDetail>>() {

    override fun loadData(param: Any?): Single<GameDetail> {
        return restHelper.loadGameDetail(param as String)
                .map {
                    logThread("loadData")
                    if (it.isSuccessful) {
                        it.body()!!
                    } else {
                        throw Exception()
                    }
                }
        // TODO: Remove and replace with offline
//                .onErrorResumeNext {
//                    Single.just(getMockedData())
//                }

    }

    private fun getMockedData(): GameDetail {
        val jordan1 = Sneakers(1, "Air Jordan 1", "https://www.flightclub" +
                ".com/media/catalog/product/cache/1/image/1600x1140/9df78eab33525d08d6e5fb8d27136e95/8/0/801781_01.jpg")
        val jordan2 = Sneakers(2, "Air Jordan 2", "https://www.flightclub" +
                ".com/media/catalog/product/cache/1/image/1600x1140/9df78eab33525d08d6e5fb8d27136e95/6/3/63597195169-air-jordan-2-retro-eminem-black-stealth-varsity-red-010955_1.jpg")
        val jordan3 = Sneakers(3, "Air Jordan 3", "https://c.static-nike" +
                ".com/a/images/t_PDP_1280_v1/f_auto/pc49nxoiung2tgw6anws/air-jordan-3-retro-shoe-89TqZyKW.jpg")
        val jordan4 = Sneakers(4, "Air Jordan 4", "https://sneakernews.com/wp-content/uploads/2017/03/jordan-4-retro-motorsports-official-images-2.jpg")

        return GameDetail("Jordans", "Jordan 1-4", arrayListOf(jordan1, jordan2, jordan3, jordan4))
    }
}