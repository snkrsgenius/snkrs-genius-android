package com.sneakersgenius.sneakersgenius.pages.score

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.view.animation.AnimationUtils
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import com.sneakersgenius.sneakersgenius.pages.BaseActivity
import com.sneakersgenius.sneakersgenius.pages.gameDetail.GameDetailActivity
import kotlinx.android.synthetic.main.score_activity_layout.*
import timber.log.Timber

/**
 * Created by Ondrej Synak on 5/13/18.
 */
class ScoreActivity : BaseActivity() {

    companion object {
        const val SCORE = "score"
        const val GAME_ID = "game_id"
        const val GAME_FINISHED = "game_finished"
        const val HIGH_SCORE = "high_score"
    }

    private lateinit var scoreViewModel: ScoreViewModel

    enum class CurrentPage {
        HIGH_SCORE,
        FINISHED_GAME,
        WRONG_ANSWER,
        TIME_OUT
    }

    private var gameId: String? = null
    private var highScore: Int = 0
    private var score: Int = 0
    private var endReason = CurrentPage.HIGH_SCORE

    /* Public Methods *****************************************************************************************************************************************/

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.score_activity_layout)

        // Decide which state bundle should be used when loading arguments.
        loadArguments(savedInstanceState ?: intent.extras)

        if (!(highScore == 0 && (endReason == CurrentPage.WRONG_ANSWER || endReason == CurrentPage.TIME_OUT))) {
            if (score > highScore) {
                highScore = score
            }
        }

        scoreViewModel = ViewModelProviders.of(this).get(ScoreViewModel::class.java)

        bindObservers()
        setupViews()
        saveScore()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onSaveInstanceState(bundle: Bundle) {
        super.onSaveInstanceState(bundle)
        bundle.putInt(SCORE, score)
        bundle.putString(GAME_ID, gameId)
        bundle.putInt(GAME_FINISHED, endReason.ordinal)
        bundle.putInt(HIGH_SCORE, highScore)
    }


    /* Private Methods ****************************************************************************************************************************************/

    private fun bindObservers() {
        Timber.v("bindObservers()")

    }

    private fun setupViews() {
        Timber.v("setupViews()")

        end_game_view_flipper.displayedChild = endReason.ordinal

        end_game_high_score_continue_button.setOnClickListener {
            onBackPressed()
        }
        end_game_finished_game_continue_button.setOnClickListener {
            restartGame()
        }
        end_game_wrong_answer_continue_button.setOnClickListener {
            restartGame()
        }
        end_game_time_out_continue_button.setOnClickListener {
            restartGame()
        }

        when (endReason) {
            CurrentPage.FINISHED_GAME -> {
                end_game_finished_game_high_score.text = "$highScore IQ"
                end_game_finished_game_score.text = "$score IQ"

                end_game_header_image_view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.header_image_anim))
                end_game_finished_game_continue_button.startAnimation(AnimationUtils.loadAnimation(this, R.anim.footer_button_anim))
                end_game_finished_game_score.startAnimation(AnimationUtils.loadAnimation(this, R.anim.big_label_text_anim))
                finished_game_score_container.startAnimation(AnimationUtils.loadAnimation(this, R.anim.headline_text_anim))

            }
            CurrentPage.HIGH_SCORE -> {
                end_game_high_score_score.text = "$highScore IQ"

                high_score_header_image_view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.header_image_anim))
                end_game_high_score_score.startAnimation(AnimationUtils.loadAnimation(this, R.anim.big_label_text_anim))
                high_score_bottom_container.startAnimation(AnimationUtils.loadAnimation(this, R.anim.footer_button_anim))
                high_score_headline_text_view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.headline_text_anim))

            }
            CurrentPage.WRONG_ANSWER -> {
                end_game_wrong_answer_high_score.text = "$highScore IQ"

                end_game_wrong_answer_continue_button.startAnimation(AnimationUtils.loadAnimation(this, R.anim.footer_button_anim))
                wrong_answer_header_image_view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.header_image_anim))
                wrong_answer_text_view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.big_label_text_anim))
                wrong_answer_score_container.startAnimation(AnimationUtils.loadAnimation(this, R.anim.headline_text_anim))
            }
            CurrentPage.TIME_OUT -> {
                end_game_time_out_high_score.text = "$highScore IQ"

                time_out_header_image_view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.header_image_anim))
                end_game_time_out_continue_button.startAnimation(AnimationUtils.loadAnimation(this, R.anim.footer_button_anim))
                time_out_text_view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.big_label_text_anim))
                time_out_score_container.startAnimation(AnimationUtils.loadAnimation(this, R.anim.headline_text_anim))
            }
        }
    }

    private fun restartGame() {
        startActivity(
                Intent(this, GameDetailActivity::class.java)
                        .putExtra(GameDetailActivity.GAME_ID, gameId)
                        .putExtra(GameDetailActivity.HIGH_SCORE, highScore)
        )
        finish()
    }

    private fun saveScore() {
        Timber.v("saveScore()")
        if (endReason == CurrentPage.HIGH_SCORE || endReason == CurrentPage.FINISHED_GAME) {
            sharedPreferencesHelper.loadString(SharedPreferencesHelper.PREF_KEY_USER_ID)?.let {
                scoreViewModel.saveData(gameId!!, it, score)
            }
        }
    }

    /**
     * Loads arguments from SavedInstance or passed bundle.
     *
     * @param state Bundle with data.
     */
    private fun loadArguments(state: Bundle?) {
        // Load arguments.
        if (state != null) {
            if (state.containsKey(ScoreActivity.GAME_ID)) {
                gameId = state.getString(ScoreActivity.GAME_ID)
            }
            if (state.containsKey(ScoreActivity.HIGH_SCORE)) {
                highScore = state.getInt(ScoreActivity.HIGH_SCORE)
            }
            if (state.containsKey(ScoreActivity.SCORE)) {
                score = state.getInt(ScoreActivity.SCORE)
            }
            if (state.containsKey(ScoreActivity.SCORE)) {
                endReason = CurrentPage.values()[state.getInt(ScoreActivity.GAME_FINISHED)]
            }
        }
    }

}