package com.sneakersgenius.sneakersgenius.pages.gameDetail

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.pages.ToolbarActivity
import com.sneakersgenius.sneakersgenius.pages.score.ScoreActivity
import com.sneakersgenius.sneakersgenius.uiComponents.DuelCardView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.game_detail_activity_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import timber.log.Timber
import java.lang.ref.WeakReference

/**
 * Created by Ondrej Synak on 5/13/18.
 */
class GameDetailActivity : ToolbarActivity() {

    companion object {
        const val GAME_ID = "game_id"
        const val HIGH_SCORE = "high_score"
    }

    /* Private Attributes ***************************************************************************/

    private lateinit var gameDetailViewModel: GameDetailViewModel
    private var gameId: String? = null
    private var highScore: Int = 0

    /* Public Methods *******************************************************************************/

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.game_detail_activity_layout)

        // Decide which state bundle should be used when loading arguments.
        loadArguments(savedInstanceState ?: intent.extras)

        setupViews()
        gameDetailViewModel = ViewModelProviders.of(this).get(GameDetailViewModel::class.java)
        gameDetailViewModel.view = WeakReference(this)
        gameDetailViewModel.highScore = highScore

        gameId?.let { gameDetailViewModel.loadData(it) }
    }

    @SuppressLint("MissingSuperCall")
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState, toolbar, true)
        title = "IQ 0"
    }

    override fun onPause() {
        Timber.d("onPause()")
        gameDetailViewModel.onPause()
        super.onPause()
        onBackPressed()
    }

    /**
     * Callback for score of right answer to show on top of cardview.
     */
    fun onFirstAnswerGood(score: Int) {
        Timber.d("onFirstAnswerGood()")
        duel_card_view.onFirstAnswerGood(score)
    }

    /**
     * Callback to show wrong answer.
     */
    fun onFirstAnswerWrong() {
        Timber.d("onFirstAnswerWrong()")
        duel_card_view.onFirstAnswerWrong()
    }

    /**
     * Callback for score of right answer to show on top of cardview.
     */
    fun onSecondAnswerGood(score: Int) {
        Timber.d("onSecondAnswerGood()")
        duel_card_view.onSecondAnswerGood(score)
    }

    /**
     * Callback to show wrong answer
     */
    fun onSecondAnswerWrong() {
        Timber.d("onSecondAnswerWrong()")
        duel_card_view.onSecondAnswerWrong()
    }

    /**
     * Setter for first image.
     */
    fun setFirstAnswerImage(image: String) {
        Timber.d("setFirstAnswerImage()")
        if (duel_card_view != null) {
            Picasso.get().load(image).into(duel_card_view.getFirstAnswerImageView())
//            duel_card_view.getFirstAnswerImageView().setImageBitmap(image)
        }
    }

    /**
     * Setter for second image.
     */
    fun setSecondAnswerImage(image: String) {
        Timber.d("setSecondAnswerImage()")
        if (duel_card_view != null) {
            Picasso.get().load(image).into(duel_card_view.getSecondAnswerImageView())
//            duel_card_view.getSecondAnswerImageView().setImageBitmap(image)
        }
    }

    /**
     * When game is stopped
     */
    fun onGameStopped(score: Int, isGameFinishedProperly: Int) {
        finish()
        startActivity(
                Intent(this, ScoreActivity::class.java)
                        .putExtra(ScoreActivity.SCORE, score)
                        .putExtra(ScoreActivity.GAME_ID, gameId)
                        .putExtra(ScoreActivity.GAME_FINISHED, isGameFinishedProperly)
                        .putExtra(ScoreActivity.HIGH_SCORE, highScore)
        )
    }

    /**
     * Setter for loader
     *
     * @param visible - visibility of loader.
     */
    fun setLoadingProgressBarVisibility(visible: Boolean) {
        Timber.d("setLoadingProgressBarVisibility()")

        duel_loading_progress_bar.visibility = if (visible) View.VISIBLE else View.GONE
    }

    /**
     * Called when images for duel failed to load.
     */
    fun onFailedImageLoading() {
        Timber.d("onFailedImageLoading")

        Toast.makeText(this, this.getString(R.string.duel_failed_to_load_images), Toast.LENGTH_LONG).show()
        gameDetailViewModel.onPause()
        super.onBackPressed()
    }

    fun onFailedLoadingGame(message: String) {
        Timber.d("onFailedLoadingGame()")

        Toast.makeText(this, message, Toast.LENGTH_LONG).show()

        gameDetailViewModel.onPause()
        super.onBackPressed()
    }

    /**
     * Clears overviews on cardview
     */
    fun clearAnswers() {
        duel_card_view?.clearAnswers()
    }

    /**
     * Setter for user interactions with cards.
     *
     * @param clickable - accessibility
     */
    fun setAnswersClickable(clickable: Boolean) {
        duel_card_view?.setAnswersClickable(clickable)
    }

    /**
     * Setter for question text
     *
     * @param questionText - text
     */
    fun setQuestionText(questionText: String) {
        Timber.d("setQuestionText() $questionText")

        duel_card_view?.setQuestionContentTextView(questionText)
    }

    /**
     * Setter for question counter
     *
     * @param questionCounter
     */
    fun setQuestionCounter(questionCounter: String) {
        Timber.d("setQuestionCounter()$questionCounter")

        duel_card_view?.setQuestionCounterTextView(questionCounter)
    }

    /**
     * Setter for progressbar which indicates time remaining.
     *
     * @param progress
     */
    fun setQuestionProgressBarTo(progress: Int, isSlow: Boolean) {
        Timber.d("setQuestionProgressBarTo()")

        duel_card_view?.setProgress(progress, isSlow)
    }


    /**
     * Setter for visibility of timer.
     *
     * @param visible
     */
    fun setQuestionProgressBarVisibility(visible: Boolean) {
        Timber.d("setQuestionProgressBarVisibility()")

        duel_card_view?.setDuelProgressBarVisibility(visible)
    }

    /**
     * Sets loading text view to inform user which image is currently loading.
     *
     * @param current
     * @param max
     */
    fun setLoadingImagesNumber(current: Int, max: Int) {
        loading_images_text_view.text = "Loading images... $current/$max"
    }

    /**
     * Clears previous question.
     */
    fun clearPreviousQuestion() {
        Timber.d("clearPreviousQuestion()")

        duel_card_view?.clearPreviousQuestion()
    }

    /**
     * Init new question
     */
    fun initNewQuestion() {
        Timber.d("initNewQuestion()")
        duel_card_view?.initNewQuestion()
    }

    /**
     * Setter for toolbar overall score.
     *
     * @param score
     */
    fun setOverallIQScore(score: Int) {
        Timber.d("setOverallIQScore() $score")

        title = "IQ $score"
    }

    /* Private Methods ****************************************************************************************************************************************/

    private fun setupViews() {
        Timber.v("setupViews")

        duel_card_view.duelResponseListener = object : DuelCardView.OnDuelResponseListener {
            override fun onPreviousQuestionCleared() {
                gameDetailViewModel.onPreviousDuelQuestionCleared()
            }

            override fun onFirstAnswerClick() {
                gameDetailViewModel.onDuelFirstCardTouched()
            }

            override fun onSecondAnswerClick() {
                gameDetailViewModel.onDuelSecondCardTouched()
            }

            override fun onNewQuestionPrepared() {
                gameDetailViewModel.onNewDuelQuestionPrepared()
            }

        }
    }

    /**
     * Loads arguments from SavedInstance or passed bundle.
     *
     * @param state Bundle with data.
     */
    private fun loadArguments(state: Bundle?) {
        // Load arguments.
        if (state != null) {
            if (state.containsKey(GameDetailActivity.GAME_ID)) {
                gameId = state.getString(GameDetailActivity.GAME_ID)
            }
            if (state.containsKey(GameDetailActivity.HIGH_SCORE)) {
                highScore = state.getInt(GameDetailActivity.HIGH_SCORE)
            }
        }
    }
}