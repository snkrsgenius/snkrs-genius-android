package com.sneakersgenius.sneakersgenius.pages.gameLeaderboard

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.Snackbar.LENGTH_INDEFINITE
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import com.sneakersgenius.sneakersgenius.network.restModels.LeaderBoardItem
import com.sneakersgenius.sneakersgenius.pages.ToolbarActivity
import com.sneakersgenius.sneakersgenius.pages.gameDetail.GameDetailActivity
import com.sneakersgenius.sneakersgenius.pages.gameTutorial.GameTutorialActivity
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.game_leaderboard_activity_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import timber.log.Timber

/**
 * Created by Ondrej Synak on 5/13/18.
 */
class GameLeaderboardActivity : ToolbarActivity() {

    companion object {
        const val GAME_ID = "game_id"
        const val GAME_NAME = "game_name"
    }

    private var gameId: String? = null
    private var gameName: String? = null
    private var playerPosition: Int = -1
    private lateinit var gameLeaderboardViewModel: GameLeaderboardViewModel
    private var leaderboardAdapter: GameLeaderboardAdapter? = null
    private var leaderBoardLayoutManager: LinearLayoutManager? = null
    private var errorSnackBar: Snackbar? = null

    /* Public Methods *****************************************************************************************************************************************/

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.game_leaderboard_activity_layout)

        // Decide which state bundle should be used when loading arguments.
        loadArguments(savedInstanceState ?: intent.extras)

        gameLeaderboardViewModel = ViewModelProviders.of(this).get(GameLeaderboardViewModel::class.java)

        setupViews()
        bindObservers()
    }

    @SuppressLint("MissingSuperCall")
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState, toolbar, true)
        gameName?.let { title = it }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString(GAME_ID, gameId)
        outState?.putString(GAME_NAME, gameName)
    }

    override fun onResume() {
        super.onResume()

        gameId?.let { gameLeaderboardViewModel.loadData(it) }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.no_anim, R.anim.slide_out_right)
    }

    /* Private Methods ****************************************************************************************************************************************/

    private fun bindObservers() {
        Timber.v("bindObservers()")

        gameLeaderboardViewModel.loadingInProgressLiveData.observe(this, Observer {
            setLoadingProgressVisibility(it!!)
        })

        gameLeaderboardViewModel.lastScoreLiveData.observe(this, Observer {
            leaderboard_latest_score_text_view.text = it.toString()
        })
        gameLeaderboardViewModel.highScoreLiveData.observe(this, Observer {
            leaderboard_my_highscore_text_view.text = it.toString()
        })
        gameLeaderboardViewModel.currentPlayerPosition.observe(this, Observer {
            playerPosition = it!!
        })

        gameLeaderboardViewModel.leaderBoardItemsLiveData.observe(this, Observer {
            if (it!!.isEmpty()) {
                leaderboard_empty_text_view.visibility = View.VISIBLE
                leaderboard_latest_score_text_view.text = "_"
                leaderboard_my_highscore_text_view.text = "_"
            } else {
                leaderboard_empty_text_view.visibility = View.GONE
                leaderboardAdapter?.setNewItems(it.toMutableList())
            }
        })

        gameLeaderboardViewModel.errorMessageVisiblityLiveData.observe(this, Observer {
            if (it!!) {
                errorSnackBar = Snackbar.make(root_view, R.string.leaderboard_error_snackbar, LENGTH_INDEFINITE)
                        .setAction(R.string.snackbar_retry, {
                            gameLeaderboardViewModel.loadData(gameId!!)
                        })
                errorSnackBar?.show()
            } else {
                errorSnackBar?.dismiss()
            }
        })
    }

    private fun setupViews() {
        Timber.v("setupViews")

        setupRecyclerView()

        start_game_button.setOnClickListener {
            if (sharedPreferencesHelper.loadBoolean(SharedPreferencesHelper.PREF_KEY_FIRST_GAME)) {

                startActivity(
                        Intent(this, GameDetailActivity::class.java)
                                .putExtra(GameDetailActivity.GAME_ID, gameId)
                                .putExtra(GameDetailActivity.HIGH_SCORE, gameLeaderboardViewModel.highScoreLiveData.value ?: 0)
                )
            } else {
                startActivity(
                        Intent(this, GameTutorialActivity::class.java)
                                .putExtra(GameTutorialActivity.GAME_NAME, gameName)
                                .putExtra(GameTutorialActivity.GAME_ID, gameId)
                                .putExtra(GameTutorialActivity.GAME_HIGHSCORE, gameLeaderboardViewModel.highScoreLiveData.value ?: 0)
                )
            }
        }
    }

    private fun setupRecyclerView() {
        leaderboardAdapter = GameLeaderboardAdapter(currentUserId = App.appComponent.provideSharedPreferencesHelper().loadString(SharedPreferencesHelper.PREF_KEY_USER_ID))
        leaderBoardLayoutManager = LinearLayoutManager(this)

        with(leaderboard_recycler_view) {
            adapter = leaderboardAdapter
            visibility = View.VISIBLE
            layoutManager = leaderBoardLayoutManager
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    Timber.d("onScrollStateChanged() ${leaderBoardLayoutManager?.findLastVisibleItemPosition()}")
                }

                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    Timber.d("onScrolled $playerPosition")
                    if (playerPosition != -1 && leaderBoardLayoutManager != null) {
                        when {
                            leaderBoardLayoutManager!!.findFirstCompletelyVisibleItemPosition() > playerPosition -> {
                                sticky_item_bottom.visibility = View.GONE
                                sticky_item_top.visibility = View.VISIBLE
                                setupStickyItem(sticky_item_top, leaderboardAdapter!!.items[playerPosition], playerPosition)

                            }
                            leaderBoardLayoutManager!!.findLastCompletelyVisibleItemPosition() < playerPosition -> {
                                sticky_item_bottom.visibility = View.VISIBLE
                                sticky_item_top.visibility = View.GONE
                                setupStickyItem(sticky_item_bottom, leaderboardAdapter!!.items[playerPosition], playerPosition)
                            }
                            else -> {
                                sticky_item_bottom.visibility = View.GONE
                                sticky_item_top.visibility = View.GONE
                            }
                        }
                    }
                }
            })
        }
    }

    private fun setLoadingProgressVisibility(visible: Boolean) {
        leaderboard_loading_progress_bar.visibility = if (visible) View.VISIBLE else View.GONE
    }

    /**
     * Setups sticky item.
     *
     * @param item
     * @param leaderBoardItem
     */
    private fun setupStickyItem(item: View?, leaderBoardItem: LeaderBoardItem?, position: Int) {
        if (item != null && leaderBoardItem != null) {
            if (position == 0) {
                item.setBackgroundResource(R.color.jay_gay_yellow_04_100)
            } else {
                item.setBackgroundResource(R.color.sneakers_transparent_green_100)
            }
            item.findViewById<TextView>(R.id.leaderboard_score_position_text_view).text = leaderboardAdapter?.getPositionString(position + 1)
            item.findViewById<TextView>(R.id.leaderboard_score_points_text_view).text = leaderBoardItem.bestScore.toString()
            item.findViewById<TextView>(R.id.score_nick_text_view).text = leaderBoardItem.player.firstName
            item.findViewById<TextView>(R.id.score_name_text_view).text = leaderBoardItem.player.lastName


            Picasso.get().load(leaderBoardItem.player.photoUrl)
                    .placeholder(R.drawable.group)
                    .into(item.findViewById<CircleImageView>(R.id.leaderboard_score_profile_image_view))
        }
    }

    /**
     * Loads arguments from SavedInstance or passed bundle.
     *
     * @param state Bundle with data.
     */
    private fun loadArguments(state: Bundle?) {
        // Load arguments.
        if (state != null) {
            if (state.containsKey(GAME_ID)) {
                gameId = state.getString(GAME_ID)
            }
            if (state.containsKey(GAME_NAME)) {
                gameName = state.getString(GAME_NAME)
            }
        }
    }
}