package com.sneakersgenius.sneakersgenius.pages.landing.components.leaderboardScroll

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.network.restModels.LeaderBoardItemLandingPage
import kotlinx.android.synthetic.main.landing_horizontal_scroll_component.view.*

/**
 * Created by Ondrej Synak on 6/9/18.
 */
class LeaderboardsLandingHorizontalScrollComponent : LinearLayout {

    private var landingLeaderboardsAdapter: LandingLeaderboardsAdapter? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
        manageAttributes(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
        manageAttributes(attrs)
    }

    fun setItems(items: MutableList<LeaderBoardItemLandingPage> = arrayListOf()) {
        landingLeaderboardsAdapter?.setNewItems(items)
    }

    fun setItemClickListener(listener: LandingLeaderboardsAdapter.OnLeaderboardsLandingItemListener) {
        landingLeaderboardsAdapter?.onLeaderboardsLandingItemListener = listener
    }

    fun setProgressBarVisibility(visible: Boolean) {
        progress_bar.visibility = if (visible) View.VISIBLE else View.GONE
    }

    /* Private Methods ******************************************************************************/

    /**
     * Initialize Widget
     */
    private fun init() {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.landing_horizontal_scroll_component, this)
        header_text_view.text = context.getString(R.string.landing_leaderboards_item_header)

        landingLeaderboardsAdapter = LandingLeaderboardsAdapter()
        with(recycler_view) {
            adapter = landingLeaderboardsAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }
    }


    private fun manageAttributes(attr: AttributeSet) {

    }
}