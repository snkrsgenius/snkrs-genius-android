package com.sneakersgenius.sneakersgenius.pages

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.widget.TextView
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import kotlinx.android.synthetic.main.toolbar_layout.*

/**
 * Created by Ondrej Synak on 5/13/18.
 */
abstract class ToolbarActivity : BaseActivity() {

    private var toolbar: Toolbar? = null

    fun onPostCreate(savedInstanceState: Bundle?, toolbar: Toolbar, isBackButtonVisible: Boolean = false) {
        super.onPostCreate(savedInstanceState)
        this.toolbar = toolbar
        setupToolbar(isBackButtonVisible)
        toolbar.post { recalculatesRightMarginOfTabLayout() }
    }

    override fun setTitle(title: CharSequence) {
        toolbar?.findViewById<TextView>(R.id.toolbar_title)?.text = title
    }

    private fun setupToolbar(isBackButtonVisible: Boolean) {
        setSupportActionBar(toolbar)

        toolbar?.title = null

        if (isBackButtonVisible) {
            toolbar?.setNavigationIcon(R.drawable.ic_arrow_back)
            toolbar?.setNavigationOnClickListener { onBackPressed() }
        }
    }

    /**
     * Recalculates right margin of tablayout because of backbutton added in toolbar layout.
     */
    private fun recalculatesRightMarginOfTabLayout() {
        toolbar?.let {
            val layoutParams = toolbar_wrapper.layoutParams as Toolbar.LayoutParams
            layoutParams.rightMargin = (it.width - toolbar_wrapper.width)
            toolbar_wrapper.layoutParams = layoutParams
        }
    }
}