package com.sneakersgenius.sneakersgenius.pages.splash

import android.annotation.SuppressLint
import android.os.Bundle
import com.sneakersgenius.sneakersgenius.pages.BaseActivity

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class SplashActivity : BaseActivity() {

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, layoutResId = null)
    }

}