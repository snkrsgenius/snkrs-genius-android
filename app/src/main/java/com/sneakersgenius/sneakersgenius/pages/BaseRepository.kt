package com.sneakersgenius.sneakersgenius.pages

import android.os.Looper
import io.reactivex.SingleSource
import timber.log.Timber

/**
 * Created by Ondrej Synak on 5/13/18.
 */
abstract class BaseRepository<out T : SingleSource<*>> {

    abstract fun loadData(param: Any? = null): T

    internal fun logThread(tag: String) {
        val currentThread = Thread.currentThread()
        Timber.v(
                "LOG Thread[%d] Priority %d, Main thread %b, %s %s %s",
                currentThread.id,
                currentThread.priority,
                currentThread == Looper.getMainLooper().thread,
                if (currentThread.isInterrupted) "INTERRUPTED" else "Running",
                currentThread.name,
                tag
        )
    }
}