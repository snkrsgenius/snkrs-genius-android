package com.sneakersgenius.sneakersgenius.pages.login

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.sneakersgenius.sneakersgenius.R
import timber.log.Timber

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class EndlessAdapter(var items: List<Int>) : RecyclerView.Adapter<EndlessAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        Timber.d("onCreateViewHolder() ")

        return ItemViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_photo, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val integer = items[position % items.size]

        holder.setImage(integer)
    }


    override fun getItemCount(): Int {
        return Integer.MAX_VALUE
    }

    fun setNewItems(newItems: List<Int>) {
        items = newItems
        notifyDataSetChanged()
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun setImage(resId: Int) {
            itemView.findViewById<ImageView>(R.id.photo).setImageResource(resId)
        }
    }
}