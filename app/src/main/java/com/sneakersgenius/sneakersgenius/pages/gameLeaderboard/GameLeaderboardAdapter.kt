package com.sneakersgenius.sneakersgenius.pages.gameLeaderboard

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.network.restModels.LeaderBoardItem
import com.sneakersgenius.sneakersgenius.utilities.OrdinalNumberUtil
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import timber.log.Timber

/**
 * Created by Ondrej Synak on 5/13/18.
 */
class GameLeaderboardAdapter(
        var items: MutableList<LeaderBoardItem> = arrayListOf(),
        private val currentUserId: String?) : RecyclerView.Adapter<GameLeaderboardAdapter.LeaderboardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaderboardViewHolder {
        return LeaderboardViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.game_leaderboard_player_item, parent, false))
    }

    override fun onBindViewHolder(holder: LeaderboardViewHolder, position: Int) {
        Timber.d("onBindViewHolder ${items.size}")
        val leaderBoardItem = items[position]

        holder.bind(leaderBoardItem, position)
    }

    /**
     * Getter for current list size
     *
     * @return - list size
     */
    override fun getItemCount(): Int = items.size

    /**
     * Setter for adapter list
     *
     * @param leaderBoardList - new list
     */
    fun setNewItems(leaderBoardList: MutableList<LeaderBoardItem>) {
        if (leaderBoardList.isNotEmpty()) {
            items = leaderBoardList
        } else {
            items.clear()
        }
        notifyDataSetChanged()
    }

    /**
     * Gets position as formatted string.
     *
     * @param position - position of player
     *
     * @return - formatted string
     */
    fun getPositionString(position: Int): String {
        val sb = StringBuilder()
        sb.append(OrdinalNumberUtil.ordinal(position))
        return sb.toString()
    }

    /**
     * ViewHolder for overview duel.
     */
    inner class LeaderboardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: LeaderBoardItem, position: Int) {
            Timber.d("bind $position $item")

            itemView.findViewById<TextView>(R.id.leaderboard_score_position_text_view).text = getPositionString(position + 1)
            itemView.findViewById<TextView>(R.id.leaderboard_score_points_text_view).text = item.bestScore.toString()
            itemView.findViewById<TextView>(R.id.score_nick_text_view).text = item.player.firstName
            itemView.findViewById<TextView>(R.id.score_name_text_view).text = item.player.lastName

            val container = itemView.findViewById<ViewGroup>(R.id.leaderboard_score_container)

            when {
                position == 0 -> {
                    container.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.jay_gay_yellow_04))
                }
                currentUserId == item.player.id -> {
                    container.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.sneakers_transparent_green_20))
                }
                else -> {
                    container.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.white))
                }
            }

            Picasso.get()
                    .load(item.player.photoUrl)
                    .placeholder(R.drawable.group)
                    .into(itemView.findViewById<CircleImageView>(R.id.leaderboard_score_profile_image_view))
        }
    }
}