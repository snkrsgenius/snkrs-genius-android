package com.sneakersgenius.sneakersgenius.pages.landing

import com.sneakersgenius.sneakersgenius.database.AppRoomDatabase
import com.sneakersgenius.sneakersgenius.network.RestHelper
import com.sneakersgenius.sneakersgenius.network.restModels.Game
import com.sneakersgenius.sneakersgenius.network.restModels.LeaderBoardItemLandingPage
import com.sneakersgenius.sneakersgenius.pages.BaseRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Ondrej Synak on 6/9/18.
 */
class LandingRepository @Inject constructor(private val restHelper: RestHelper, private val appRoomDatabase: AppRoomDatabase) {

    fun loadGameData(): Single<List<Game>> {

        return restHelper.loadMostPlayedGames()
                .map {
                    if (it.isSuccessful) {
                        it.body()!!.forEach {
                            it.players?.sort()
                        }
                        it.body()!!
                    } else {
                        throw Exception()
                    }
                }
                .onErrorResumeNext {
                    if (appRoomDatabase.provideGameEntityDao().count() == 0L) {
                        throw Exception()
                    } else {
                        throw Exception()
                    }
                }
    }

    fun loadLeaderboardsData(userId: String): Single<List<LeaderBoardItemLandingPage>> {

        return restHelper.loadLandingLeaderboardsForUser(userId)
                .map {
                    if (it.isSuccessful) {
                        it.body()!!
                    } else {
                        throw Exception()
                    }
                }
                .onErrorResumeNext {
                    if (appRoomDatabase.provideGameEntityDao().count() == 0L) {
                        throw Exception()
                    } else {
                        throw Exception()
                    }
                }
    }
}