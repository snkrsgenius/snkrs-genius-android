package com.sneakersgenius.sneakersgenius.pages.landing

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import com.sneakersgenius.sneakersgenius.network.restModels.Game
import com.sneakersgenius.sneakersgenius.network.restModels.LeaderBoardItemLandingPage
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Ondrej Synak on 6/9/18.
 */
class LandingViewModel : ViewModel() {
    init {
        App.appComponent.inject(this)
    }

    @Inject
    lateinit var landingRepository: LandingRepository
    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper

    val gamesLiveData: MutableLiveData<List<Game>> = MutableLiveData()
    val leaderboardLiveData: MutableLiveData<List<LeaderBoardItemLandingPage>> = MutableLiveData()
    val errorVisibility: MutableLiveData<Boolean> = MutableLiveData()

    /* Private Attributes *************************************************************************************************************************************/

    private var gameDataLoadingDisposable: Disposable? = null
    private var leaderboardsDataLoadingDisposable: Disposable? = null

    /* Public Methods *****************************************************************************************************************************************/

    fun loadData() {
        errorVisibility.postValue(false)

        gameDataLoadingDisposable?.dispose()

        gameDataLoadingDisposable = landingRepository.loadGameData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    gamesLiveData.postValue(it)
                }, {
                    Timber.e(it)
                    errorVisibility.postValue(true)
                })

        sharedPreferencesHelper.loadString(SharedPreferencesHelper.PREF_KEY_USER_ID)?.let {
            leaderboardsDataLoadingDisposable = landingRepository.loadLeaderboardsData(it)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        leaderboardLiveData.postValue(it)
                    }, {
                        Timber.e(it)
                        errorVisibility.postValue(true)
                    })
        }
    }

    override fun onCleared() {
        gameDataLoadingDisposable?.dispose()
        leaderboardsDataLoadingDisposable?.dispose()

        super.onCleared()
    }
}