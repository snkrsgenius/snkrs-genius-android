package com.sneakersgenius.sneakersgenius.pages.gameOverview

import com.sneakersgenius.sneakersgenius.database.AppRoomDatabase
import com.sneakersgenius.sneakersgenius.network.RestHelper
import com.sneakersgenius.sneakersgenius.network.restModels.Game
import com.sneakersgenius.sneakersgenius.pages.BaseRepository
import io.reactivex.Single
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 5/12/18.
 */
@Singleton
class GameOverviewRepository @Inject constructor(private val restHelper: RestHelper, private val appRoomDatabase: AppRoomDatabase) : BaseRepository<Single<List<Game>>>() {

    override fun loadData(param: Any?): Single<List<Game>> {

        return restHelper.loadGames()
                .map {
                    logThread("loadGames")
                    if (it.isSuccessful) {
                        it.body()!!.forEach {
                            it.players?.sort()
                        }
                        it.body()!!
                    } else {
                        throw Exception()
                    }
                }
                .onErrorResumeNext {
                    if (appRoomDatabase.provideGameEntityDao().count() == 0L) {
                        throw Exception()
                    } else {
                        Single.just(getMockedData())
                    }
                }
    }

    fun getMockedData(): List<Game> {
        return arrayListOf(
                Game("1", "Prvni", "Prvni hra", "https://cdn.britannica.com/668x448/09/188709-004-EE0B39A7.jpg", ""),
                Game("2", "Druha", "Druha hra", "http://cdn.hoopshype.com/i/8a/17/b0/jordan_600.jpg", "")
        )
    }
}