package com.sneakersgenius.sneakersgenius.pages.gameLeaderboard

import com.sneakersgenius.sneakersgenius.network.RestHelper
import com.sneakersgenius.sneakersgenius.network.restModels.LeaderBoardItem
import com.sneakersgenius.sneakersgenius.network.restModels.User
import com.sneakersgenius.sneakersgenius.pages.BaseRepository
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 5/13/18.
 */
@Singleton
class GameLeaderboardRepository @Inject constructor(private val restHelper: RestHelper) : BaseRepository<Single<List<LeaderBoardItem>>>() {

    override fun loadData(param: Any?): Single<List<LeaderBoardItem>> {
        return restHelper.loadLeaderBoardForGame(param as String)
                .map {
                    logThread("loadData")
                    if (it.isSuccessful) {
                        it.body()!!
                    } else {
                        throw Exception()
                    }
                }
                // TODO: Remove and replace with offline
//                .onErrorResumeNext {
//                    Single.just(getMockedData())
//                }
    }

    fun getMockedData(): List<LeaderBoardItem> {
        val user1 = User("Jakub", "jakub.sobotka@mail.com", "JAkub", "Sobotka", "https://scontent.fprg2-1.fna.fbcdn.net/v/t1.0-1/c80.0.320.320/p320x320/10685427_713506825364910_1509519881141083521_n.jpg?_nc_cat=0&oh=ce764cd0fa843c01079b25a948cb8528&oe=5B8C1EDE")
        val user2 = User("Ondrej", "ondrej.sobotka@mail.com", "Ondrej", "Synak", "https://scontent.fprg2-1.fna.fbcdn.net/v/t1.0-1/c80.0.320.320/p320x320/10685427_713506825364910_1509519881141083521_n.jpg?_nc_cat=0&oh=ce764cd0fa843c01079b25a948cb8528&oe=5B8C1EDE")
        val user3 = User("Ondrej1", "ondrej.sobotka@mail.com", "Ondrej", "Synak", "https://scontent.fprg2-1.fna.fbcdn.net/v/t1.0-1/c80.0.320.320/p320x320/10685427_713506825364910_1509519881141083521_n.jpg?_nc_cat=0&oh=ce764cd0fa843c01079b25a948cb8528&oe=5B8C1EDE")
        val user4 = User("Ondrej2", "ondrej.sobotka@mail.com", "Ondrej", "Synak", "https://scontent.fprg2-1.fna.fbcdn.net/v/t1.0-1/c80.0.320.320/p320x320/10685427_713506825364910_1509519881141083521_n.jpg?_nc_cat=0&oh=ce764cd0fa843c01079b25a948cb8528&oe=5B8C1EDE")
        val user5 = User("Ondrej3", "ondrej.sobotka@mail.com", "Ondrej", "Synak", "https://scontent.fprg2-1.fna.fbcdn.net/v/t1.0-1/c80.0.320.320/p320x320/10685427_713506825364910_1509519881141083521_n.jpg?_nc_cat=0&oh=ce764cd0fa843c01079b25a948cb8528&oe=5B8C1EDE")
        val user6 = User("Ondrej4", "ondrej.sobotka@mail.com", "Ondrej", "Synak", "https://scontent.fprg2-1.fna.fbcdn.net/v/t1.0-1/c80.0.320.320/p320x320/10685427_713506825364910_1509519881141083521_n.jpg?_nc_cat=0&oh=ce764cd0fa843c01079b25a948cb8528&oe=5B8C1EDE")
        val user7 = User("Ondrej5", "ondrej.sobotka@mail.com", "Ondrej", "Synak", "https://scontent.fprg2-1.fna.fbcdn.net/v/t1.0-1/c80.0.320.320/p320x320/10685427_713506825364910_1509519881141083521_n.jpg?_nc_cat=0&oh=ce764cd0fa843c01079b25a948cb8528&oe=5B8C1EDE")
        val user8 = User("Ondrej6", "ondrej.sobotka@mail.com", "Ondrej", "Synak", "https://scontent.fprg2-1.fna.fbcdn.net/v/t1.0-1/c80.0.320.320/p320x320/10685427_713506825364910_1509519881141083521_n.jpg?_nc_cat=0&oh=ce764cd0fa843c01079b25a948cb8528&oe=5B8C1EDE")


        return arrayListOf(
                LeaderBoardItem(user1, 500, 20, "1", 1),
                LeaderBoardItem(user2, 400, 30, "1", 2),
                LeaderBoardItem(user4, 200, 30, "1", 4),
                LeaderBoardItem(user5, 150, 30, "1", 5),
                LeaderBoardItem(user3, 300, 30, "1", 3),
                LeaderBoardItem(user6, 100, 30, "1", 6),
                LeaderBoardItem(user8, 80, 30, "1", 8),
                LeaderBoardItem(user7, 90, 30, "1", 7)
        )
    }
}