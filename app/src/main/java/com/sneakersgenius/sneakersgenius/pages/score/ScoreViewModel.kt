package com.sneakersgenius.sneakersgenius.pages.score

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.network.restModels.GameScore
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * Created by Ondrej Synak on 5/13/18.
 */
class ScoreViewModel : ViewModel() {

    init {
        App.appComponent.inject(this)
    }

    @Inject
    lateinit var scoreRepository: ScoreRepository

    val loadingInProgressLiveData: MutableLiveData<Boolean> = MutableLiveData()

    /* Private Attributes *************************************************************************************************************************************/

    private var dataLoadingDisposable: Disposable? = null

    /* Public Methods *****************************************************************************************************************************************/

    fun saveData(gameId: String, playerId: String, score: Int) {
        dataLoadingDisposable?.dispose()

        dataLoadingDisposable = scoreRepository.saveData(gameId, GameScore(score, playerId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }

    override fun onCleared() {
        dataLoadingDisposable?.dispose()

        super.onCleared()
    }
}