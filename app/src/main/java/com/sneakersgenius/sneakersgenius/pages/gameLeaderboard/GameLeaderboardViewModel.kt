package com.sneakersgenius.sneakersgenius.pages.gameLeaderboard

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import com.sneakersgenius.sneakersgenius.network.restModels.LeaderBoardItem
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.util.*
import javax.inject.Inject

/**
 * Created by Ondrej Synak on 5/13/18.
 */
class GameLeaderboardViewModel : ViewModel() {

    init {
        App.appComponent.inject(this)
    }

    @Inject
    lateinit var gameLeaderboardRepository: GameLeaderboardRepository
    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper

    val leaderBoardItemsLiveData: MutableLiveData<List<LeaderBoardItem>> = MutableLiveData()
    val highScoreLiveData: MutableLiveData<Int> = MutableLiveData()
    val lastScoreLiveData: MutableLiveData<Int> = MutableLiveData()
    val currentPlayerPosition: MutableLiveData<Int> = MutableLiveData()
    val loadingInProgressLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val errorMessageVisiblityLiveData: MutableLiveData<Boolean> = MutableLiveData()

    /* Private Attributes *************************************************************************************************************************************/

    private var dataLoadingDisposable: Disposable? = null

    /* Public Methods *****************************************************************************************************************************************/

    fun loadData(gameId: String) {
        loadingInProgressLiveData.postValue(true)
        errorMessageVisiblityLiveData.postValue(false)

        dataLoadingDisposable?.dispose()

        dataLoadingDisposable = gameLeaderboardRepository
                .loadData(gameId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    val mutableResult = result.toMutableList()
                    mutableResult.sort()
                    leaderBoardItemsLiveData.postValue(mutableResult)
                    loadingInProgressLiveData.postValue(false)

                    val currentUserItem = mutableResult.firstOrNull { it.player.id == sharedPreferencesHelper.loadString(SharedPreferencesHelper.PREF_KEY_USER_ID) }
                    currentUserItem?.let {
                        highScoreLiveData.postValue(it.bestScore)
                        lastScoreLiveData.postValue(it.lastScore)
                        currentPlayerPosition.postValue(mutableResult.indexOf(it))
                    }

                }, {
                    loadingInProgressLiveData.postValue(false)
                    errorMessageVisiblityLiveData.postValue(true)
                    Timber.e(it)
                })
    }

    override fun onCleared() {
        dataLoadingDisposable?.dispose()

        super.onCleared()
    }
}