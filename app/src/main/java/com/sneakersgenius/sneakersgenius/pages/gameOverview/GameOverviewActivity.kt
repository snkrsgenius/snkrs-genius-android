package com.sneakersgenius.sneakersgenius.pages.gameOverview

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import com.sneakersgenius.sneakersgenius.network.restModels.Game
import com.sneakersgenius.sneakersgenius.pages.ToolbarActivity
import com.sneakersgenius.sneakersgenius.pages.gameLeaderboard.GameLeaderboardActivity
import com.sneakersgenius.sneakersgenius.uiComponents.DotsIndicator
import com.sneakersgenius.sneakersgenius.utilities.MiscUtils
import kotlinx.android.synthetic.main.game_overview_activity_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import timber.log.Timber

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class GameOverviewActivity : ToolbarActivity() {

    private var overviewAdapter: GameOverviewAdapter? = null
    private val onGameOverviewItemListener = object : GameOverviewAdapter.OnGameOverviewItemListener {
        override fun onItemClicked(game: Game) {
            Timber.v("onItemClicked() $game")
            startActivity(
                    Intent(this@GameOverviewActivity, GameLeaderboardActivity::class.java)
                            .putExtra(GameLeaderboardActivity.GAME_NAME, game.name)
                            .putExtra(GameLeaderboardActivity.GAME_ID, game.id)
            )

            overridePendingTransition(R.anim.slide_in_right, R.anim.no_anim)
        }
    }

    private lateinit var gameOverviewViewModel: GameOverviewViewModel
    private var errorSnackBar: Snackbar? = null
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var dotsIndicator: DotsIndicator? = null

    /* Public Methods *****************************************************************************************************************************************/

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.game_overview_activity_layout)

        gameOverviewViewModel = ViewModelProviders.of(this).get(GameOverviewViewModel::class.java)

        bindObservers()
        setupViews()
    }

    @SuppressLint("MissingSuperCall")
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState, toolbar, true)
        title = getString(R.string.overview_duel_title)
    }

    override fun onResume() {
        super.onResume()

        gameOverviewViewModel.loadData()

        dots_container.post {
            updateDotsIndicatorActivePosition()
        }
    }

    /* Private Methods ****************************************************************************************************************************************/

    private fun bindObservers() {
        Timber.v("bindObservers()")

        gameOverviewViewModel.loadingInProgressLiveData.observe(this, Observer {
            setLoadingProgressVisibility(it!!)
        })

        gameOverviewViewModel.gamesLiveData.observe(this, Observer {
            overviewAdapter?.setNewGames(it!!)
            dotsIndicator?.setDots(it!!.size)
            dotsIndicator?.initDots()
            updateDotsIndicatorActivePosition()
        })

        gameOverviewViewModel.errorVisibility.observe(this, Observer {
            if (it!!) {
                errorSnackBar = Snackbar.make(root_view, R.string.leaderboard_error_snackbar, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.snackbar_retry, {
                            gameOverviewViewModel.loadData()
                        })
                errorSnackBar?.show()
            } else {
                errorSnackBar?.dismiss()
            }
        })
    }

    private fun setupViews() {
        Timber.v("setupViews")

        overviewAdapter = GameOverviewAdapter(onGameOverviewItemListener = onGameOverviewItemListener, userId = App.appComponent.provideSharedPreferencesHelper().loadString
        (SharedPreferencesHelper
                .PREF_KEY_USER_ID)!!)

        linearLayoutManager = LinearLayoutManager(this@GameOverviewActivity, LinearLayoutManager.HORIZONTAL, false)
        with(overview_recycler_view) {
            adapter = overviewAdapter
            visibility = View.VISIBLE
            addItemDecoration(SpacesItemDecoration(MiscUtils.dpToPx(28)))
            layoutManager = linearLayoutManager
            setItemViewCacheSize(2)
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    val pos = linearLayoutManager.findFirstVisibleItemPosition()

                    dotsIndicator?.onScroll(pos, layoutManager.findViewByPosition(pos).right)
                }
            })
        }

        dotsIndicator = DotsIndicator(dots_container)
        dotsIndicator?.init()

    }

    private fun setLoadingProgressVisibility(visible: Boolean) {
        overview_loading_progress_bar.visibility = if (visible) View.VISIBLE else View.GONE
    }

    private fun updateDotsIndicatorActivePosition() {
        if (linearLayoutManager.findFirstVisibleItemPosition() != 0) {
            dotsIndicator?.updateDots(linearLayoutManager.findFirstVisibleItemPosition(), true)
        }
    }

    /**
     * Items decoration for regular spacing.
     */
    inner class SpacesItemDecoration(private val halfSpace: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View,
                                    parent: RecyclerView, state: RecyclerView.State?) {

            parent.setPadding(halfSpace, 0, halfSpace, 0)
            parent.clipToPadding = false

            outRect.top = 0
            outRect.bottom = 0
            outRect.left = halfSpace
            outRect.right = halfSpace
        }
    }
}