package com.sneakersgenius.sneakersgenius.utilities

import android.animation.Animator

/**
 * Created by Ondrej Synak on 5/13/18.
 */
open class ExtendedAnimatorListener : Animator.AnimatorListener {

    override fun onAnimationRepeat(animation: Animator?) {

    }

    override fun onAnimationEnd(animation: Animator?) {

    }

    override fun onAnimationCancel(animation: Animator?) {

    }

    override fun onAnimationStart(animation: Animator?) {

    }
}
