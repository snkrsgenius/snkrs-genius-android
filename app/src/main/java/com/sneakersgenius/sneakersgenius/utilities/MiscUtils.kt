package com.sneakersgenius.sneakersgenius.utilities

import android.content.Context
import android.content.res.Resources
import android.graphics.Point
import android.util.DisplayMetrics
import android.view.WindowManager
import timber.log.Timber
import java.io.IOException
import java.io.InputStream

/**
 * Created by Ondrej Synak on 5/12/18.
 */
/**
 * Useful static methods.
 */
object MiscUtils {

    /**
     * Converts PX to DP on primary screen.
     *
     * @param px PX to convert.
     *
     * @return Resulting DP.
     */
    fun pxToDp(context: Context, px: Int): Int {
        return (px / context.resources.displayMetrics.density + 0.5).toInt()
    }

    /**
     * Converts DP to PX on primary screen.
     *
     * @param dp DP to convert.
     *
     * @return Resulting PX.
     */
    fun dpToPx(context: Context, dp: Int): Int {
        return (dp * context.resources.displayMetrics.density + 0.5).toInt()
    }

    /**
     * Returns country code of the current locale.
     */
    fun getCountryCode(context: Context): String {
        return context.resources.configuration.locale.country
    }

    /**
     * @see Resources.getString
     */
    fun getResourceString(context: Context, id: Int): String {
        return context.resources.getString(id)
    }

    /**
     * @see Resources.getString
     */
    fun getResourceString(context: Context, resourceId: Int, vararg formatArgs: Any): String {
        return context.resources.getString(resourceId, *formatArgs)
    }

    /**
     * @see Resources.getBoolean
     */
    fun getResourceBoolean(context: Context, id: Int): Boolean {
        return context.resources.getBoolean(id)
    }

    /**
     * @see Resources.getDimensionPixelSize
     */
    fun getResourceDimension(context: Context, id: Int): Int {
        return context.resources.getDimensionPixelSize(id)
    }

    /**
     * Opens stream of specified asset file.
     *
     * @param assetFileName Asset file name.
     *
     * @return Opened stream of asset file.
     */
    fun getAssetFileStream(context: Context, assetFileName: String): InputStream? {
        var result: InputStream? = null
        try {
            result = context.assets.open(assetFileName)
        } catch (exception: IOException) {
            Timber.e(exception.message, exception)
        }

        return result
    }

    fun getDisplaySize(windowManager: WindowManager): Point {
        return try {
            val display = windowManager.defaultDisplay
            val displayMetrics = DisplayMetrics()
            display.getMetrics(displayMetrics)
            Point(displayMetrics.widthPixels, displayMetrics.heightPixels)
        } catch (e: Exception) {
            e.printStackTrace()
            Point(0, 0)
        }

    }

    fun dpToPx(dp: Int): Int {
        return (dp * Resources.getSystem().displayMetrics.density).toInt()
    }
}