package com.sneakersgenius.sneakersgenius.utilities

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import android.support.annotation.RequiresApi

/**
 * Created by Ondrej Synak on 6/10/18.
 */
/**
 * This treasure was created by Frantisek Spurny (frantisek.spurny@master.cz).
 */
@RequiresApi(Build.VERSION_CODES.O)
class NotificationChannels(context: Context) : ContextWrapper(context) {

    companion object {
        const val DEFAULT_CHANNEL_ID = "com.sneakersgenius.sneakersgenius.default"
        const val DEFAULT_CHANNEL_NAME = "DEFAULT CHANNEL"
    }

    private val manager = getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager

    init {
        manager.createNotificationChannel(NotificationChannel(
                DEFAULT_CHANNEL_ID,
                DEFAULT_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH))
    }
}