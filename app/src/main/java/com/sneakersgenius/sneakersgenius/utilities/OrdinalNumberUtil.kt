package com.sneakersgenius.sneakersgenius.utilities

/**
 * Created by Ondrej Synak on 5/12/18.
 */
object OrdinalNumberUtil {

    /**
     * Converts number to ordinal string.
     * @param i - number
     * @return - string with st,nd,rd,th,...
     */
    fun ordinal(i: Int): String {
        val sufixes = arrayOf("th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th")
        return when (i % 100) {
            11, 12, 13 -> i.toString() + "th"
            else -> i.toString() + sufixes[i % 10]
        }
    }
}
