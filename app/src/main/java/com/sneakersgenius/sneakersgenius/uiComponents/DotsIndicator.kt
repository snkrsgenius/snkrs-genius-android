package com.sneakersgenius.sneakersgenius.uiComponents

import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import com.sneakersgenius.sneakersgenius.R

/**
 * Created by Ondrej Synak on 5/20/18.
 */
class DotsIndicator(val rootLayout: FrameLayout) {
    private lateinit var dotsContainer: LinearLayout
    private var savedPos: Int = 0

    init {
        savedPos = -1
    }

    fun init() {
        val params = FrameLayout.LayoutParams(-2, -2)
        params.gravity = 17
        dotsContainer = LinearLayout(rootLayout.context)
        dotsContainer.layoutParams = params
        rootLayout.addView(dotsContainer)
    }

    fun initDots() {
        this.savedPos = -1
        this.updateDots(0)
    }

    fun updateDots(pos: Int, forceUpdate: Boolean = false) {
        if (this.savedPos != pos || forceUpdate) {
            this.savedPos = pos

            for (i in 0 until dotsContainer.childCount) {
                (dotsContainer.getChildAt(i) as ImageView).setImageResource(if (i == pos) R.drawable.dot_indicator_active else R.drawable.dot_indicator_inactive)
            }

        }
    }

    fun setDots(itemCount: Int) {
        if (itemCount > 0) {
            dotsContainer.removeAllViews()
            val params = android.widget.LinearLayout.LayoutParams(-2, -2)
            params.setMargins(3, 3, 3, 3)

            for (i in 0 until itemCount) {
                val imageView = ImageView(dotsContainer.context)
                imageView.setImageResource(R.drawable.dot_indicator_inactive)
                imageView.layoutParams = params
                dotsContainer.addView(imageView)
            }

        }
    }

    fun setVisibility(visibility: Int) {
        rootLayout.visibility = visibility
    }

    internal fun onScroll(pos: Int, right: Int) {
        var position = pos
        if (right < dotsContainer.context.resources.displayMetrics.widthPixels / 2) {
            ++position
        }

        this.updateDots(position)
    }
}