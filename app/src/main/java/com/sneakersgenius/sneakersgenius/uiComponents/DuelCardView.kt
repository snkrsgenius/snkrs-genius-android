package com.sneakersgenius.sneakersgenius.uiComponents

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.BounceInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.FrameLayout
import android.widget.ImageView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.utilities.ExtendedAnimatorListener
import kotlinx.android.synthetic.main.game_detail_duel_card_view.view.*
import timber.log.Timber

/**
 * Created by Ondrej Synak on 5/13/18.
 */

class DuelCardView : FrameLayout {

    companion object {
        private const val FLIP_IN_DURATION = 500
        private const val ZOOM_IN_DURATION = 300
        private const val PROGRESS_MULTIPLIER = 1_000
    }

    /* Private Attributes ***************************************************************************/

    var duelResponseListener: OnDuelResponseListener? = null
        set(duelResponseListener) {
            Timber.d("setDuelResponseListener()")
            field = duelResponseListener
        }

    private var question: String? = null

    /* Public Attributes ****************************************************************************/


    /* Public Methods *******************************************************************************/

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
        manageAttributes(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
        manageAttributes(attrs)
    }

    fun getFirstAnswerImageView(): ImageView {
        return duel_first_image_view
    }

    fun getSecondAnswerImageView(): ImageView = duel_second_image_view

    fun setQuestionContentTextView(questionContent: String?) {
        question = questionContent
    }

    fun displayQuestionContentTextView() {
        duel_question_content_text_view.text = question
    }

    fun setQuestionCounterTextView(questionCounter: String) {
        duel_question_counter_text_view.text = questionCounter
    }

    /**
     * Displays green overlay with score over second image.
     *
     * @param score
     */
    fun onFirstAnswerGood(score: Int) {
        clearAnswers()
        duel_first_overview.setBackgroundColor(ContextCompat.getColor(context, R.color.sneakers_duel_good))
        duel_first_points_text_view.text = "+$score"
    }

    /**
     * Displays red overlay over first image
     */
    fun onFirstAnswerWrong() {
        clearAnswers()
        duel_first_overview.setBackgroundColor(ContextCompat.getColor(context, R.color.sneakers_duel_wrong))
    }

    /**
     * Displays green overlay with score over second image.
     *
     * @param score
     */
    fun onSecondAnswerGood(score: Int) {
        clearAnswers()
        duel_second_overview.setBackgroundColor(ContextCompat.getColor(context, R.color.sneakers_duel_good))
        duel_second_points_text_view.text = "+$score"
    }

    /**
     * Displays red overlay over second image
     */
    fun onSecondAnswerWrong() {
        clearAnswers()
        duel_second_overview.setBackgroundColor(ContextCompat.getColor(context, R.color.sneakers_duel_wrong))
    }

    /**
     * Init new question. Animate it in.
     */
    fun initNewQuestion() {
        Timber.d("initNewQuestion()")

        YoYo.with(Techniques.FlipInX)
                .duration(FLIP_IN_DURATION.toLong())
                .withListener(object : ExtendedAnimatorListener() {
                    override fun onAnimationEnd(animation: Animator?) {
                        YoYo.with(Techniques.ZoomIn)
                                .duration(ZOOM_IN_DURATION.toLong())
                                .playOn(duel_first_question_card_view)

                        YoYo.with(Techniques.ZoomIn)
                                .duration(ZOOM_IN_DURATION.toLong())
                                .delay(50)
                                .withListener(object : ExtendedAnimatorListener() {
                                    override fun onAnimationStart(animation: Animator?) {
                                        // Need to be here because reset set target to alpha 1.
                                        duel_second_question_card_view.alpha = 0f
                                    }

                                    override fun onAnimationEnd(animation: Animator?) {
                                        duelResponseListener!!.onNewQuestionPrepared()
                                    }
                                })
                                .playOn(duel_second_question_card_view)
                    }

                    override fun onAnimationStart(animation: Animator?) {
                        displayQuestionContentTextView()
                    }

                })
                .playOn(duel_question_container)
    }

    /**
     * Animates out previous question.
     */
    fun clearPreviousQuestion() {
        Timber.d("clearPreviousQuestion()")

        YoYo.with(Techniques.SlideOutLeft)
                .duration(FLIP_IN_DURATION.toLong())
                .withListener(object : ExtendedAnimatorListener() {
                    override fun onAnimationEnd(animation: Animator?) {
                        duelResponseListener!!.onPreviousQuestionCleared()
                    }
                })
                .playOn(duel_question_container)

        YoYo.with(Techniques.SlideOutLeft)
                .duration(FLIP_IN_DURATION.toLong())
                .playOn(duel_first_question_card_view)

        YoYo.with(Techniques.SlideOutLeft)
                .duration(FLIP_IN_DURATION.toLong())
                .playOn(duel_second_question_card_view)
    }

    /**
     * Sets progress on progress bar
     *
     * @param progress
     * @param isSlow if animation sould be slow or fast
     */
    fun setProgress(progress: Int, isSlow: Boolean) {
        val animation = ObjectAnimator
                .ofInt(duel_time_progress, "progress", duel_time_progress!!.progress, progress * PROGRESS_MULTIPLIER)
        animation.duration = (if (isSlow) 1000 else 350).toLong()
        animation.interpolator = if (isSlow) BounceInterpolator() else OvershootInterpolator(3.0f)
        animation.start()
    }


    /**
     * Setter for user accessibility
     *
     * @param clickable
     */
    fun setAnswersClickable(clickable: Boolean) {
        if (clickable) {
            duel_first_question_card_view.isClickable = true
            duel_second_question_card_view.isClickable = true
        } else {
            duel_first_question_card_view.isClickable = false
            duel_second_question_card_view.isClickable = false
        }
    }

    /**
     * Setter for timer visibility
     *
     * @param visible
     */
    fun setDuelProgressBarVisibility(visible: Boolean) {
        duel_time_progress.visibility = if (visible) View.VISIBLE else View.INVISIBLE
    }

    /**
     * Clears answers overlays
     */
    fun clearAnswers() {
        duel_first_overview.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent))
        duel_second_overview.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent))
        duel_first_points_text_view.text = ""
        duel_second_points_text_view.text = ""
    }

    /* Private Methods ******************************************************************************/

    private fun manageAttributes(attr: AttributeSet) {

        val attributes = context.obtainStyledAttributes(attr, R.styleable.DuelCardLayout)

        try {

            setQuestionContentTextView(attributes.getString(R.styleable.DuelCardLayout_questionText))

            duel_question_content_text_view.setTextColor(attributes.getColor(R.styleable.DuelCardLayout_questionColor, Color.WHITE))
        } finally {
            attributes.recycle()
        }
    }

    /**
     * Initialize Widget
     */
    private fun init() {

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.game_detail_duel_card_view, this)

        duel_first_question_card_view.setOnClickListener {
            duelResponseListener?.let {
                duel_first_question_card_view.isClickable = false
                it.onFirstAnswerClick()
            }
        }

        duel_second_question_card_view.setOnClickListener {
            duelResponseListener?.let {
                duel_second_question_card_view.isClickable = false
                it.onSecondAnswerClick()
            }
        }
    }


    /**
     * Interface for callback from DuelCardWidget
     */
    interface OnDuelResponseListener {

        fun onPreviousQuestionCleared()

        fun onFirstAnswerClick()

        fun onSecondAnswerClick()

        fun onNewQuestionPrepared()
    }
}