package com.sneakersgenius.sneakersgenius.uiComponents

import android.content.Context
import android.graphics.Point
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.WindowManager
import timber.log.Timber

/**
 * Created by Ondrej Synak on 5/12/18.
 */

class AlphaChangingCardView : CardView {

    /* Private Attributes ***************************************************************************/

    /**
     * Event fired when scrolling
     */
    private var onScrollChangedListener: ViewTreeObserver.OnScrollChangedListener? = null

    /**
     * Event for global layout changes
     */
    private var onGlobalLayoutListener: ViewTreeObserver.OnGlobalLayoutListener? = null

    /**
     * Screen width in pixels.
     */
    private var screenWidth: Int = 0

    /**
     * Initial height of widget.
     */
    private var widgetHeight = 0

    /* Public Methods *******************************************************************************/

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        Timber.d("onAttachedToWindow()")

        // Init listeners and bind them.
        onScrollChangedListener = ViewTreeObserver.OnScrollChangedListener {
            applyAlpha()
            applyTransformation()
        }

        onGlobalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
            applyAlpha()
        }

        val viewTreeObserver = viewTreeObserver

        viewTreeObserver.addOnScrollChangedListener(onScrollChangedListener)
        viewTreeObserver.addOnGlobalLayoutListener(onGlobalLayoutListener)

        initAlphaChanging()
    }

    override fun onDetachedFromWindow() {
        Timber.d("onDetachedFromWindow()")

        val viewTreeObserver = viewTreeObserver
        viewTreeObserver.removeOnScrollChangedListener(onScrollChangedListener)
        viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener)
        super.onDetachedFromWindow()
    }


    /* Private Methods ******************************************************************************/

    /**
     * Initialize alpha changing
     */
    private fun initAlphaChanging() {
        getScreenWidth()
        applyAlpha()
    }

    /**
     * Save Screen width
     */
    private fun getScreenWidth() {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        // Gets display size.
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)

        screenWidth = size.x
    }

    /**
     * Applies alpha depends on location on screen
     */
    private fun applyAlpha() {
        val location = IntArray(2)
        // Get location on screen.
        getLocationOnScreen(location)

        // Calculates center of card view on screen.
        val centerLocationOfViewOnScreen = location[0] + width / 2

        // Absolute position of card view from center of screen.
        val absoluteLocationOfViewOnScreen = Math.abs(screenWidth / 2 - centerLocationOfViewOnScreen).toFloat()

        // Percentage alpha from position of card view.
        val percentage = absoluteLocationOfViewOnScreen / (screenWidth / 2f * 2.2f)

        // Inverse value of percentage alpha.
        alpha = 1 - percentage
    }

    /**
     * Applies transfromation of whole view.
     */
    private fun applyTransformation() {

        // Get max height.
        if (widgetHeight < height) {
            widgetHeight = height
        }

        val location = IntArray(2)
        // Get location on screen.
        getLocationOnScreen(location)

        val centerLocationOfViewOnScreen = location[0] + width / 2

        // Absolute position of card view from center of screen.
        val absoluteLocationOfViewOnScreen = Math.abs(screenWidth / 2 - centerLocationOfViewOnScreen).toFloat()

        // If height is already calculated, adjust it to location on screen.
        val params = layoutParams as ViewGroup.MarginLayoutParams
        if (height != 0) {
            val height = Math.round(widgetHeight * (1 - absoluteLocationOfViewOnScreen / (screenWidth / 2f * 18f)))
            params.height = height
            params.setMargins(params.leftMargin, (widgetHeight - height) / 2, params.rightMargin,
                    params.bottomMargin)

            requestLayout()
        }
    }
}