package com.sneakersgenius.sneakersgenius.uiComponents

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.sneakersgenius.sneakersgenius.App
import com.sneakersgenius.sneakersgenius.R
import com.sneakersgenius.sneakersgenius.utilities.OrdinalNumberUtil
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.leaderboard_item.view.*

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class GameOverviewPlayerLinearLayout : LinearLayout {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    fun clearView() {
        leaderboard_user_image_view.setImageDrawable(null)
        score_nick_text_view.text = "No Player"
        leaderboard_user_position_text_view.text = "_"
        leaderboard_user_points_text_view.text = "_"
    }

    /**
     * Sets player image
     * @param url
     */
    fun setPlayerImage(url: String?) {
        if (url == null) {
            leaderboard_user_image_view.setImageDrawable(null)
        } else {
            Picasso.get().load(url).placeholder(R.drawable.group).into(leaderboard_user_image_view)
        }
    }

    /**
     * Setter for player name.
     * @param name
     */
    fun setPlayerName(name: String) {
        score_nick_text_view.text = name
    }

    /**
     * Setter for player position
     * @param position
     */
    fun setPlayerPosition(position: Int) {
        leaderboard_user_position_text_view.text = getPositionString(position)
    }

    /**
     * Setter for player score.
     * @param score
     */
    fun setPlayerScore(score: Int) {
        leaderboard_user_points_text_view.text = score.toString()
    }


    /**
     * Initialize Widget
     */
    private fun init() {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.leaderboard_item, this)
    }

    /**
     * Gets position as formatted string.
     * @param position - position of player
     * @return - formatted string
     */
    private fun getPositionString(position: Int): String {
        val sb = StringBuilder()
        sb.append(OrdinalNumberUtil.ordinal(position))
        return sb.toString()
    }
}
