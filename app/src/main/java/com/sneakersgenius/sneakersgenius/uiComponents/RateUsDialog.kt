package com.sneakersgenius.sneakersgenius.uiComponents

import android.app.Dialog
import android.content.Context
import android.support.v4.content.ContextCompat
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import com.sneakersgenius.sneakersgenius.R
import kotlinx.android.synthetic.main.rate_dialog_layout.*

/**
 * Created by Ondrej Synak on 5/27/18.
 */
class RateUsDialog(context: Context) : Dialog(context) {


    fun setOnRateButtonClickListener(onClickListener: View.OnClickListener) {
        rate_us_button.setOnClickListener(onClickListener)
    }

    fun setOnLAterClickListener(onClickListener: View.OnClickListener) {
        later_button.setOnClickListener(onClickListener)
    }

    private fun init() {

        setContentView(R.layout.rate_dialog_layout)

        //Grab the window of the dialog, and change the width
        val layoutParams = WindowManager.LayoutParams()
        window!!.setBackgroundDrawableResource(android.R.color.transparent)
        layoutParams.copyFrom(window.attributes)

        //This makes the dialog take up the full width
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.gravity = Gravity.BOTTOM
        window.attributes = layoutParams

        val headerText = context.getString(R.string.rate_dialog_header)
        val styledText = SpannableString(headerText.toUpperCase())
        styledText.setSpan(ForegroundColorSpan(ContextCompat.getColor(context, R.color.jay_gay_yellow)), 16, headerText.length, Spannable
                .SPAN_EXCLUSIVE_EXCLUSIVE)

        rate_us_header_text_view.text = styledText
    }

    init {
        init()
    }
}