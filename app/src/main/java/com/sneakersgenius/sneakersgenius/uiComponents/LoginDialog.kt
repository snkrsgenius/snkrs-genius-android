package com.sneakersgenius.sneakersgenius.uiComponents

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import com.sneakersgenius.sneakersgenius.R
import kotlinx.android.synthetic.main.login_dialog.*

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class LoginDialog : Dialog {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, themeResId: Int) : super(context, themeResId) {
        init()
    }

    constructor(context: Context, cancelable: Boolean,
                cancelListener: DialogInterface.OnCancelListener) : super(context, cancelable, cancelListener) {
        init()
    }

    private fun init() {

        setContentView(R.layout.login_dialog)

        //Grab the window of the dialog, and change the width
        val layoutParams = WindowManager.LayoutParams()
        window!!.setBackgroundDrawableResource(android.R.color.transparent)
        layoutParams.copyFrom(window.attributes)

        //This makes the dialog take up the full width
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.gravity = Gravity.BOTTOM
        window.attributes = layoutParams
    }

    fun setOnOkButtonClickListener(onClickListener: View.OnClickListener) {
        login_dialog_ok_button.setOnClickListener(onClickListener)
    }

    fun setOnLogOutClickListener(onClickListener: View.OnClickListener) {
        login_dialog_logout_button.setOnClickListener(onClickListener)
    }

    fun setOkButtonTitle(name: String) {
        login_dialog_ok_button.text = name
    }

    fun setTitle(title: String) {
        login_dialog_title.text = title
    }
}
