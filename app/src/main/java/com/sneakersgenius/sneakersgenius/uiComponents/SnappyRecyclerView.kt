package com.sneakersgenius.sneakersgenius.uiComponents

import android.content.Context
import android.content.res.Resources
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet

/**
 * Created by Ondrej Synak on 5/12/18.
 */
class SnappyRecyclerView : RecyclerView {

    // Use it with a horizontal LinearLayoutManager
    // Based on http://stackoverflow.com/a/29171652/4034572
    private var overallXScroll = 0

    // Returns the currently snapped item position
    val firstVisibleItemPosition: Int
        get() {
            val linearLayoutManager = layoutManager as LinearLayoutManager
            return linearLayoutManager.findFirstCompletelyVisibleItemPosition()
        }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }

    private fun init() {
        overallXScroll = 0
    }

    override fun onScrolled(dx: Int, dy: Int) {
        super.onScrolled(dx, dy)
        overallXScroll += dx
    }

    override fun fling(velocityX: Int, velocityY: Int): Boolean {

        val linearLayoutManager = layoutManager as LinearLayoutManager

        val screenWidth = Resources.getSystem().displayMetrics.widthPixels

        // views on the screen
        val lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition()
        val lastView = linearLayoutManager.findViewByPosition(lastVisibleItemPosition)
        val firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
        val firstView = linearLayoutManager.findViewByPosition(firstVisibleItemPosition)

        if (lastView == null || firstView == null) {
            return false
        }

        // distance we need to scroll
        val leftMargin = (screenWidth - lastView.width) / 2
        val rightMargin = (screenWidth - firstView.width) / 2 + firstView.width
        val leftEdge = lastView.left
        val rightEdge = firstView.right
        val scrollDistanceLeft = leftEdge - leftMargin
        val scrollDistanceRight = rightMargin - rightEdge

        return if (Math.abs(velocityX) < 100) {
            // The fling is slow -> stay at the current page if we are less than half through,
            // or go to the next page if more than half through

            false
        } else {
            // The fling is fast -> go to next page

            if (velocityX > 0) {
                smoothScrollBy(scrollDistanceLeft, 0)
            } else {
                smoothScrollBy(-scrollDistanceRight, 0)
            }
            true
        }


    }

    override fun onScrollStateChanged(state: Int) {
        super.onScrollStateChanged(state)

        // If you tap on the phone while the RecyclerView is scrolling it will stop in the middle.
        // This code fixes this. This code is not strictly necessary but it improves the behaviour.

        if (state == RecyclerView.SCROLL_STATE_IDLE && adapter.itemCount > 0) {
            val linearLayoutManager = layoutManager as LinearLayoutManager

            val screenWidth = Resources.getSystem().displayMetrics.widthPixels

            // views on the screen
            val lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition()
            val lastView = linearLayoutManager.findViewByPosition(lastVisibleItemPosition)
            val firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
            val firstView = linearLayoutManager.findViewByPosition(firstVisibleItemPosition)

            // distance we need to scroll
            val leftMargin = (screenWidth - lastView.width) / 2
            val rightMargin = (screenWidth - firstView.width) / 2 + firstView.width
            val leftEdge = lastView.left
            val rightEdge = firstView.right
            val scrollDistanceLeft = leftEdge - leftMargin
            val scrollDistanceRight = rightMargin - rightEdge

            if (leftEdge > screenWidth / 2) {
                smoothScrollBy(-scrollDistanceRight, 0)
            } else if (rightEdge < screenWidth / 2) {
                smoothScrollBy(scrollDistanceLeft, 0)
            }
        }
    }
}