package com.sneakersgenius.sneakersgenius

import android.app.Application
import android.content.Context
import android.os.StrictMode
import com.sneakersgenius.sneakersgenius.crashlytics.CrashlyticsTree
import com.sneakersgenius.sneakersgenius.dependencyInjection.SharedPreferencesHelper
import com.sneakersgenius.sneakersgenius.dependencyInjection.component.AppComponent
import com.sneakersgenius.sneakersgenius.dependencyInjection.component.DaggerAppComponent
import com.sneakersgenius.sneakersgenius.dependencyInjection.module.AppModule
import com.sneakersgenius.sneakersgenius.dependencyInjection.module.DatabaseModule
import com.sneakersgenius.sneakersgenius.dependencyInjection.module.NetworkModule
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import timber.log.Timber
import java.lang.ref.WeakReference
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import uk.co.chrisjenx.calligraphy.CalligraphyConfig


/**
 * Created by Ondrej Synak on 5/12/18.
 */
class App : Application() {

    companion object {
        val restApiServerUrl = when {
            true -> "https://surevpn.masterinter.net/sneakersGenius/"
            else -> "http://10.0.2.2:8080/sneakersGenius/"
        }
        lateinit var appComponent: AppComponent
        lateinit var contextReference: WeakReference<Context>
    }

    override fun onCreate() {
        super.onCreate()

        contextReference = WeakReference(this)

        initializeCalligraphy()
        initializeTimber()
        initializeStrictMode()
        initializeDependencyInjectionComponents()

        if (appComponent.provideSharedPreferencesHelper().loadLong(SharedPreferencesHelper.PREF_KEY_RATE_LATER) == -1L) {
            appComponent.provideSharedPreferencesHelper().saveLong(SharedPreferencesHelper.PREF_KEY_RATE_LATER, DateTime.now(DateTimeZone.UTC).plusDays(2).millis)
        }

        Fabric.with(this, Crashlytics())
    }

    private fun initializeTimber() {
        Timber.plant(Timber.DebugTree(), CrashlyticsTree())
    }

    private fun initializeStrictMode() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build())
            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build())
        }
    }

    private fun initializeDependencyInjectionComponents() {
        appComponent = DaggerAppComponent.builder()
                .networkModule(NetworkModule(restApiServerUrl))
                .databaseModule(DatabaseModule())
                .appModule(AppModule(this))
                .build()
    }

    private fun initializeCalligraphy() {
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Futura Bold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build())
    }
}