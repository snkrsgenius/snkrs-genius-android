package com.sneakersgenius.sneakersgenius.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.sneakersgenius.sneakersgenius.database.entities.SneakerEntity

/**
 * Created by Ondrej Synak on 5/15/18.
 */
@Dao
interface SneakerEntityDao {
    @Query("DELETE FROM sneakerEntity")
    fun deleteAll()

    @Query("DELETE FROM sneakerEntity WHERE sneakerEntity.id = :id")
    fun deleteById(id: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(sneakerEntity: SneakerEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(sneakerEntities: List<SneakerEntity>): LongArray
}