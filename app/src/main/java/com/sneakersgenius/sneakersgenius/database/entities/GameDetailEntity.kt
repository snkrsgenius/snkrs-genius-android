package com.sneakersgenius.sneakersgenius.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Ondrej Synak on 5/15/18.
 */
@Entity(tableName = "gameDetailEntity",
        foreignKeys = [
            (ForeignKey(entity = GameEntity::class, parentColumns = arrayOf("id"), childColumns = arrayOf("gameId")))
        ])
data class GameDetailEntity(@PrimaryKey(autoGenerate = false) var id: String,
                            var name: String,
                            var description: String,
                            var gameId: String)