package com.sneakersgenius.sneakersgenius.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import com.sneakersgenius.sneakersgenius.network.restModels.LeaderBoardItem

/**
 * Created by Ondrej Synak on 5/15/18.
 */
@Entity(tableName = "leaderBoardItemEntity",
        foreignKeys = [
            (ForeignKey(entity = GameEntity::class, parentColumns = arrayOf("id"), childColumns = arrayOf("gameId"))),
            (ForeignKey(entity = UserEntity::class, parentColumns = arrayOf("id"), childColumns = arrayOf("playerId")))
        ])
data class LeaderBoardItemEntity(@PrimaryKey(autoGenerate = false) var id: String,
                                 var playerId: String,
                                 var bestScore: Int,
                                 var lastScore: Int,
                                 var gameId: String,
                                 var position: Int) : Comparable<LeaderBoardItem> {
    override fun compareTo(other: LeaderBoardItem): Int {
        return other.bestScore.compareTo(bestScore)
    }
}