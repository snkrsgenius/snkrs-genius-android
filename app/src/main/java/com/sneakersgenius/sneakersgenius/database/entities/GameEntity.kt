package com.sneakersgenius.sneakersgenius.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Ondrej Synak on 5/15/18.
 */
@Entity(tableName = "gameEntity")
data class GameEntity(@PrimaryKey(autoGenerate = false) var id: String,
                      var name: String,
                      var description: String,
                      var image: String,
                      var imageByte: ByteArray)