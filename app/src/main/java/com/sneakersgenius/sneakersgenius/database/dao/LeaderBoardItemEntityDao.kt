package com.sneakersgenius.sneakersgenius.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.sneakersgenius.sneakersgenius.database.entities.LeaderBoardItemEntity

/**
 * Created by Ondrej Synak on 5/15/18.
 */
@Dao
interface LeaderBoardItemEntityDao {

    @Query("DELETE FROM leaderBoardItemEntity")
    fun deleteAll()

    @Query("DELETE FROM leaderBoardItemEntity WHERE leaderBoardItemEntity.id = :id")
    fun deleteById(id: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(leaderBoardItemEntity: LeaderBoardItemEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(leaderBoardItemEntities: List<LeaderBoardItemEntity>): LongArray
}