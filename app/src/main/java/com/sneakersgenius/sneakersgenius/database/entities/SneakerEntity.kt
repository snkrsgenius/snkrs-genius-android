package com.sneakersgenius.sneakersgenius.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Ondrej Synak on 5/15/18.
 */
@Entity(tableName = "sneakerEntity",
        foreignKeys = [
            (ForeignKey(entity = GameDetailEntity::class, parentColumns = arrayOf("id"), childColumns = arrayOf("gameDetailId")))
        ])
data class SneakerEntity(@PrimaryKey(autoGenerate = false) var id: String,
                         var name: String,
                         var photoUrl: String,
                         var photoBytes: ByteArray,
                         var gameDetailId: String)