package com.sneakersgenius.sneakersgenius.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.sneakersgenius.sneakersgenius.database.dao.*
import com.sneakersgenius.sneakersgenius.database.entities.*

/**
 * Created by Ondrej Synak on 5/15/18.
 */
@Database(entities = [UserEntity::class, SneakerEntity::class, LeaderBoardItemEntity::class, GameEntity::class, GameDetailEntity::class],
        version = 1, exportSchema = false)

abstract class AppRoomDatabase : RoomDatabase() {

    abstract fun provideUserEntityDao(): UserEntityDao

    abstract fun provideSneakerEntityDao(): SneakerEntityDao

    abstract fun provideLeaderBoardItemEntityDao(): LeaderBoardItemEntityDao

    abstract fun provideGameEntityDao(): GameEntityDao

    abstract fun provideGameDetailEntityDao(): GameDetailEntityDao
}
