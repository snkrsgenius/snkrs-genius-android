package com.sneakersgenius.sneakersgenius.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.sneakersgenius.sneakersgenius.database.entities.GameDetailEntity

/**
 * Created by Ondrej Synak on 5/15/18.
 */
@Dao
interface GameDetailEntityDao {

    @Query("DELETE FROM gameDetailEntity")
    fun deleteAll()

    @Query("DELETE FROM gameDetailEntity WHERE gameDetailEntity.id = :id")
    fun deleteById(id: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveGameDetailEntity(gameDetailEntity: GameDetailEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(gameDetailEntities: List<GameDetailEntity>): LongArray
}