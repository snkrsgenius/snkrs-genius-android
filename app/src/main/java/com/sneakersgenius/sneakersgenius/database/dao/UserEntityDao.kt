package com.sneakersgenius.sneakersgenius.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.sneakersgenius.sneakersgenius.database.entities.UserEntity

/**
 * Created by Ondrej Synak on 5/15/18.
 */
@Dao
interface UserEntityDao {
    @Query("DELETE FROM userEntity")
    fun deleteAll()

    @Query("DELETE FROM userEntity WHERE userEntity.id = :id")
    fun deleteById(id: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(userEntity: UserEntity): Long
}