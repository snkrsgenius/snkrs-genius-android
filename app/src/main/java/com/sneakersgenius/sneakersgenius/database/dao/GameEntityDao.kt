package com.sneakersgenius.sneakersgenius.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.sneakersgenius.sneakersgenius.database.entities.GameEntity

/**
 * Created by Ondrej Synak on 5/15/18.
 */
@Dao
interface GameEntityDao {
    @Query("DELETE FROM gameEntity")
    fun deleteAll()

    @Query("DELETE FROM gameEntity WHERE gameEntity.id = :id")
    fun deleteById(id: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveGameEntity(gameEntity: GameEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(gameEntities: List<GameEntity>): LongArray

    @Query("SELECT COUNT(*) FROM gameEntity")
    fun count(): Long
}