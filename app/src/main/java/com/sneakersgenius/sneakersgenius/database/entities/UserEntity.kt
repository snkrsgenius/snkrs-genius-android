package com.sneakersgenius.sneakersgenius.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Ondrej Synak on 5/15/18.
 */
@Entity(tableName = "userEntity")
data class UserEntity (@PrimaryKey(autoGenerate = false) var id: String,
                       var email: String,
                       var firstName: String,
                       var lastName: String,
                       var pictureUrl: String,
                       var token: String? = null)